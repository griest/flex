declare module 'graphql-s2s' {
  namespace graphqls2s {
    export function transpileSchema(schema: string): string;
  }
}
