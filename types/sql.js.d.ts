import type {InitSqlJsStatic} from 'sql.js';

declare global {
  const initSqlJs: InitSqlJsStatic;
}
