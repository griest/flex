declare module '*.gql' {
  import {DocumentNode} from 'graphql';
  const Schema: DocumentNode;

  export = Schema;
}

declare module '*.gqls' {
  import {DocumentNode} from 'graphql';
  const Schema: DocumentNode;

  export = Schema;
}

declare module '*.gqlschema' {
  const Schema: string;

  export = Schema;
}
