const {transpileSchema} = require('graphql-s2s').graphqls2s;
const fs = require('fs');

module.exports = function (source: string) {
  const options = (this as any).getOptions();
  const schema = transpileSchema(source);
  const logger = (this as any).getLogger('graphql-s2s-loader');
  logger.debug(schema);
  if (options?.logFile) {
    fs.writeFileSync(options.logFile, schema);
  }
  return schema;
};
