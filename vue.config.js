const {defineConfig} = require('@vue/cli-service');
const path = require('path');

const libPath = path.resolve(__dirname, 'src/lib');

module.exports = defineConfig({
  transpileDependencies: true,

  pluginOptions: {
    vuetify: {
      // https://github.com/vuetifyjs/vuetify-loader/tree/next/packages/vuetify-loader
    },
  },

  configureWebpack: {
    resolve: {
      alias: {
        '#': libPath,
        '@': path.resolve(__dirname, 'src/components'),
        '~': path.resolve(__dirname, 'src'),
        '%': path.resolve(__dirname, 'static'),
      },
    },

    externals: {
      'sql.js': 'initSqlJs',
    },

    // entry: {
    //   lib: {
    //     // import: [
    //     //   'src/library',
    //     // ],
    //     // filename: 'lib.js',
    //   },
    // },
  },
});
