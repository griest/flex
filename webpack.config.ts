const {merge} = require('webpack-merge');
const path = require('path');
const fs = require('fs');
const CircularDependencyPlugin = require('circular-dependency-plugin');
const FilterWarningsPlugin = require('webpack-filter-warnings-plugin');
const ts = require('typescript');

let counter = 0;

const transformer = function (context: any) {
  const visitor = function (node: any) {
    fs.writeFileSync(path.resolve(__dirname, 'dump', `${counter}.json`), node.toString());
    counter++;

    return ts.visitEachChild(node, visitor, context);
  };
  return function (node: any) { return ts.visitNode(node, visitor); };
};

const baseConfig = {
  devtool: 'source-map',
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        loader: 'ts-loader',
        exclude: /node_modules/,
        options: {
          getCustomTransformers: () => ({
            before: [transformer],
          }),
        },
      },
      {
        test: /\.gqlschema$/,
        exclude: /node_modules/,
        use: 'raw-loader',
      },
      {
        test: /\.(graphql|gql)$/,
        exclude: /node_modules/,
        use: 'graphql-tag/loader',
      },
      {
        test: /\.(graphqls|gqls)$/,
        exclude: /node_modules/,
        use: [
          'graphql-tag/loader',
          {
            loader: path.resolve(__dirname, 'webpack/graphql-s2s.loader.ts'),
            options: {
              logFile: path.resolve(__dirname, 'debug.gql'),
            },
          },
        ],
      },
    ],
  },
  resolve: {
    extensions: ['.js', '.ts'],
    alias: {
      '#': path.resolve(__dirname, 'src/lib'),
      '@': path.resolve(__dirname, 'src/components'),
      '~': path.resolve(__dirname, 'src'),
      '%': path.resolve(__dirname, 'static'),
    },
  },
  resolveLoader: {
    modules: ['node_modules', path.resolve(__dirname, 'webpack')],
  },
  plugins: [
    new CircularDependencyPlugin({
      // exclude detection of files based on a RegExp
      exclude: /node_modules/,
      // include specific files based on a RegExp
      include: /src\/lib/,
      // add errors to webpack instead of warnings
      failOnError: false,
      // allow import cycles that include an asyncronous import,
      // e.g. via import(/* webpackMode: "weak" */ './file.js')
      allowAsyncCycles: false,
      // set the current working directory for displaying module paths
      cwd: process.cwd(),
    }),
  ],
};

module.exports = [
  merge(baseConfig, {
    name: 'lib',
    entry: './src/lib/main.ts',
    // target: 'node',
    mode: 'development',
    devtool: 'source-map',
    output: {
      filename: 'lib.js',
      path: path.resolve(__dirname, 'dist'),
      library: {
        name: 'flex',
        type: 'umd',
      },
    },
  }),
  merge(baseConfig, {
    name: 'server',
    entry: './src/server/main.ts',
    target: 'node',
    mode: 'development',
    devtool: 'source-map',
    output: {
      filename: 'server.js',
      path: path.resolve(__dirname, '.output'),
    },
    plugins: [
      new FilterWarningsPlugin({
        exclude: [/mongodb/, /mssql/, /mysql/, /mysql2/, /oracledb/, /react-native-sqlite-storage/, /redis/, /sqlite3/, /sql\.js/, /typeorm-aurora-data-api-driver/, /@sap\/hana-client/, /hdb-pool/],
      }),
    ],
    // dependencies: ['lib'],
  }),
];
