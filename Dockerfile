FROM node:lts

RUN ["npm", "i", "-g", "nodemon"]
RUN ["npm", "i", "-g", "source-map-support"]
ENV NODE_PATH="/usr/local/lib/node_modules"

WORKDIR /home/node

COPY .output/server.js .

EXPOSE 3000

CMD [ "node", "server.js" ]
