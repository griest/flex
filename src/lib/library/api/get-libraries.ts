import {FLEX_DATABASE_SOURCE} from '#/core/database';
import type {RequestHandler} from 'express';
import Container from 'typedi';
import {FlexLibraryEntity} from '#/library/database';
import {FlexLibrary} from '#/library';

export const flexLibraryAPIGetLibraries: RequestHandler<Record<string, unknown>, FlexLibrary[]> = async (_req, res) => {
  const connection = Container.get(FLEX_DATABASE_SOURCE);
  const userRepository = connection.getRepository(FlexLibraryEntity);
  const libraries = await userRepository.find({
    relations: ['items', 'items.media', 'items.metadata.cast', 'items.metadata.crew'],
  });
  res.send(libraries);
};
