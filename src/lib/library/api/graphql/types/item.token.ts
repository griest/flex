import {createScopedSingleToken} from '#/core/di';

export const {
  token: FLEX_LIBRARY_API_GRAPHQL_ITEM_TYPE,
  provide: provideLibraryApiGraphQlItemType,
  inject: injectLibraryApiGraphQlItemType,
} = createScopedSingleToken<string>('FLEX_LIBRARY_API_GRAPHQL_ITEM_TYPE');
