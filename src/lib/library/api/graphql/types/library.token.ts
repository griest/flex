import {createScopedSingleToken} from '#/core/di';

export const {
  token: FLEX_LIBRARY_API_GRAPHQL_TYPE,
  provide: provideLibraryApiGraphQlType,
  inject: injectLibraryApiGraphQlType,
} = createScopedSingleToken<string>('FLEX_LIBRARY_API_GRAPHQL_TYPE');
