import FlexLibraryGraphQlSchema from './schema.gqlschema';
import {provideApiGraphQlSchema} from '#/api/graphql';
import '#/api/graphql/registration';
import '#/media/api/graphql/registration';
import '#/metadata/api/graphql/registration';

provideApiGraphQlSchema(FlexLibraryGraphQlSchema);
