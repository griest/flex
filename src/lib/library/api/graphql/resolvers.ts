import {FlexAddLibraryInput, FlexLibrary, FlexLibraryItem} from '#/library';
import {FlexLibraryEntity, injectFlexLibraryDatabaseEntity} from '#/library/database';
import {flexServerAPIGraphQlRootValue} from '#/server/api/graphql';
import {FLEX_DATABASE_SOURCE} from '#/core/database';
import {IResolvers} from '@graphql-tools/utils';
import Container from 'typedi';
import {injectLibraryApiGraphQlItemType, injectLibraryApiGraphQlType} from './types';
import {GraphQLResolveInfo} from 'graphql';
import {parseResolveInfo} from 'graphql-parse-resolve-info';
import {FlexApiGraphQlContext} from '#/api/graphql';

export const flexLibraryGraphQlResolvers: IResolvers<typeof flexServerAPIGraphQlRootValue, FlexApiGraphQlContext> = {
  Query: {
    async libraries (obj, args, {db}, info: GraphQLResolveInfo) {
      // TODO: add filtering
      return await db.transaction(async transactionalEntityManager => {
        const repo = transactionalEntityManager.getRepository(FlexLibraryEntity);
        const libraries = await repo.find({
          relations: ['items', 'shows'],
        });
        const parsedInfo = parseResolveInfo(info);
        // db.getMetadata(injectFlexLibraryDatabaseEntity(obj));

        return libraries;
      });
    },
  },
  Mutation: {
    // async addLibrary(obj, args: FlexAddLibraryInput) {
    //   const connection = Container.get(FLEX_DATABASE_SOURCE);
    //   const repo = connection.getRepository(FlexLibraryEntity);
    //   const library = repo.create(args);

    //   await repo.save(library);
    //   return await repo.find();
    // },
  },
  LibraryInterface: {
    __resolveType (obj: FlexLibrary) {
      return injectLibraryApiGraphQlType(obj.type);
    },
  },
  LibraryItemInterface: {
    __resolveType (obj: FlexLibraryItem) {
      return injectLibraryApiGraphQlItemType(obj.type);
    },
  },
};
