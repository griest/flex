import {FlexMediaSource} from '#/media';
import {FlexMediaSourceEntity} from '#/media/database';
import {FLEX_DATABASE_SOURCE} from '#/core/database';
import {Response, Request} from 'express';
import {STATUS_CODES} from 'http';
import Container from 'typedi';
import {FlexLibraryItemEntity} from '../database';
import {FlexLibraryItem} from '../models';
export const flexLibraryAPIGetLink = async (
  req: Request<
  Record<string, unknown>,
  string,
  {item: FlexLibraryItem['uid']; source?: FlexMediaSource['uid']}
  >,
  res: Response,
): Promise<void> => {
  const connection = Container.get(FLEX_DATABASE_SOURCE);
  const repo = connection.getRepository(FlexLibraryItemEntity);
  const item = await repo.findOneOrFail({
    where: {
      uid: req.query.id as string,
    },
    relations: ['library'],
  });
  const dirs = item.library.scanOptions.directories;
  const source = item.media?.sources[0];

  if (!source) {
    res.status(405).send('The specified library item is not playable.');
  } else {
    // TODO: get real hostname
    res.send(`http://localhost:3000/source/movies/${source.file.path.slice(dirs[0].length).join('/')}`);
  }
};
