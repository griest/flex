import {FLEX_DATABASE_SOURCE} from '#/core/database';
import {Response, Request} from 'express';
import Container from 'typedi';
import {injectFlexLibraryDatabaseEntity, injectLibraryScanIngress} from '#/library/database';
import {FlexLibrary, FlexAddLibraryInput, flexLibraryValidateRegisteredType} from '#/library';
import {flexLibraryAPIScanLibrary} from './scan-library';

export const flexLibraryAPIAddLibrary = async (
  req: Request<
  Record<string, unknown>,
  FlexLibrary[],
  FlexAddLibraryInput
  >,
  res: Response,
): Promise<void> => {
  flexLibraryValidateRegisteredType(req.body.type);

  const connection = Container.get(FLEX_DATABASE_SOURCE);
  const entity = injectFlexLibraryDatabaseEntity(req.body.type);
  const libraryRepository = connection.getRepository(entity);
  const library = libraryRepository.create(req.body);
  await libraryRepository.save(library);

  flexLibraryAPIScanLibrary(library.uid).then(async files => {
    injectLibraryScanIngress(req.body.type)?.(library.uid, files);
  });

  res.send(await libraryRepository.findOneOrFail({
    where: {
      uid: library.uid,
    },
  }));
};
