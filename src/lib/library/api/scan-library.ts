import {FlexFSScanService} from '#/fs/server';
import {FlexLibraryEntity, injectFlexLibraryDatabaseEntity, injectLibraryScanIngress} from '#/library/database';
import {FLEX_DATABASE_SOURCE, injectFlexDatabaseSource} from '#/core/database';
import type {Request, Response} from 'express';
import Container from 'typedi';
import {FlexLibrary, FLEX_LIBRARY_SCAN_OPTIONS} from '#/library';

// TODO: stream file results
export const flexLibraryAPIScanLibrary = async (libraryId: FlexLibrary['uid']) => {
  const connection = Container.get(FLEX_DATABASE_SOURCE);
  const repo = connection.getRepository(FlexLibraryEntity);
  const driver = Container.get(FlexFSScanService);
  const library = await repo.findOne({
    where: {
      uid: libraryId,
    },
  });
  if (!library) {
    throw new Error('Library not found');
  }
  const defaultScanOptions = Container.of(library.type).get(FLEX_LIBRARY_SCAN_OPTIONS);

  return driver.scan(library.scanOptions.directories || defaultScanOptions.directories, {
    extensions: library.scanOptions.extensions || defaultScanOptions.extensions,
    minSize: library.scanOptions.minSize || defaultScanOptions.minSize,
  });
};

export const flexLibraryAPIScan = async (req: Request, res: Response) => {
  // useFlexFSServerDrivers()
  if (!req.body.libraryId) {
    res.send('No library specified!');
  }
  const dataSource = injectFlexDatabaseSource();
  const libraryRepository = dataSource.getRepository(injectFlexLibraryDatabaseEntity(req.body.type));
  const library = await libraryRepository.findOneOrFail({
    where: {
      uid: req.body.libraryId,
    },
  });
  flexLibraryAPIScanLibrary(library.uid).then(async files => {
    injectLibraryScanIngress(library.type)?.(library.uid, files);
  });
  res.send('Scan in progress...');
};
