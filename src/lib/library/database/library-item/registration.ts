import {FLEX_TYPE_DEFAULT} from '#/core';
import {FlexLibraryDatabaseItemEntityTypeRegistration, FLEX_LIBRARY_DATABASE_ITEM_ENTITY_REGISTRATIONS} from './token';
import {FlexLibraryItemEntity} from './entity.class';
import Container from 'typedi';
import {provideFlexDatabaseEntities} from '#/core/database';

export const flexLibraryDatabaseItemRegistration: FlexLibraryDatabaseItemEntityTypeRegistration<FlexLibraryItemEntity> = {
  type: FLEX_TYPE_DEFAULT,
  entity: FlexLibraryItemEntity,
};

Container.set<typeof flexLibraryDatabaseItemRegistration>({
  id: FLEX_LIBRARY_DATABASE_ITEM_ENTITY_REGISTRATIONS,
  value: flexLibraryDatabaseItemRegistration,
  multiple: true,
});
provideFlexDatabaseEntities(FlexLibraryItemEntity);
