import {Constructable, Token} from 'typedi';
import type {FlexLibraryItemEntityBase} from './entity.class';

export interface FlexLibraryDatabaseItemEntityTypeRegistration<
  Entity extends FlexLibraryItemEntityBase = FlexLibraryItemEntityBase,
> {
  type: string;
  entity?: Constructable<Entity>;
}

export const FLEX_LIBRARY_DATABASE_ITEM_ENTITY_REGISTRATIONS = new Token<FlexLibraryDatabaseItemEntityTypeRegistration>('FLEX_LIBRARY_DATABASE_ITEM_ENTITY_REGISTRATIONS');
