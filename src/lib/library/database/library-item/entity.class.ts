import {FlexTypeableEntity} from '#/core/database';
import {FlexLibraryItem} from '#/library';
import {FlexMediaItemEntityBase} from '#/media/database';
import type {FlexMetadataEntity} from '#/metadata/database';
import {Column, Entity, JoinColumn, ManyToOne, OneToOne, TableInheritance} from 'typeorm';
import type {FlexLibraryEntityBase} from '../library';

export abstract class FlexLibraryItemEntityBase extends FlexTypeableEntity<FlexLibraryItem> implements FlexLibraryItem {
  @OneToOne<FlexMediaItemEntityBase>('FlexMediaItemEntityBase', {
    cascade: true,
    // eager: true,
    nullable: true,
  })
  @JoinColumn()
  media?: FlexMediaItemEntityBase;

  @ManyToOne<FlexLibraryEntityBase>('FlexLibraryEntityBase', library => library.items)
  library!: FlexLibraryEntityBase;

  @OneToOne<FlexMetadataEntity>('FlexMetadataEntity', {
    cascade: true,
    eager: true,
  })
  @JoinColumn()
  metadata?: FlexMetadataEntity;

  @Column()
  name!: string;
}

@Entity()
@TableInheritance({column: {name: 'entityType', type: 'varchar'}})
export class FlexLibraryItemEntity extends FlexLibraryItemEntityBase {}
