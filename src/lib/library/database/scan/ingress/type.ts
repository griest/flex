import {FlexFSFile} from '#/fs';
import {FlexLibraryEntityBase} from '../../library';

export type FlexLibraryScanIngress<TLibrary extends FlexLibraryEntityBase = FlexLibraryEntityBase> = (libraryId: TLibrary['uid'], files: FlexFSFile[]) => Promise<TLibrary>;
