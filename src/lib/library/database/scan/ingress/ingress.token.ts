import {createScopedSingleToken} from '#/core/di';
import type {FlexLibraryScanIngress} from './type';

export const {
  token: FLEX_LIBRARY_SCAN_INGRESS,
  provide: provideLibraryScanIngress,
  inject: injectLibraryScanIngress,
} = createScopedSingleToken<FlexLibraryScanIngress>('FLEX_LIBRARY_SCAN_INGRESS');
