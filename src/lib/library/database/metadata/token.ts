import Container, {Token} from 'typedi';
import type {FlexLibraryDatabaseMetadataProcessor} from './processor.type';

export const FLEX_LIBRARY_DATABASE_METADATA_PROCESSOR = new Token<FlexLibraryDatabaseMetadataProcessor>('FLEX_LIBRARY_DATABASE_METADATA_PROCESSOR');

export const provideFlexLibraryDatabaseMetadataProcessor = (libraryType: string, processor: FlexLibraryDatabaseMetadataProcessor) => {
  Container.of(libraryType).set({
    id: FLEX_LIBRARY_DATABASE_METADATA_PROCESSOR,
    value: processor,
  });
};
