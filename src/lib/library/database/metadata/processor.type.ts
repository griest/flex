import {FlexLibraryEntity} from '../library';

export type FlexLibraryDatabaseMetadataProcessor<T extends FlexLibraryEntity = FlexLibraryEntity> = (library: T) => Promise<void>;
