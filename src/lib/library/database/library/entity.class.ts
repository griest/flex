import {FlexTypeableEntity} from '#/core/database';
import type {FlexLibrary, FlexLibraryScanOptions} from '#/library';
import {Column, Entity, OneToMany, TableInheritance} from 'typeorm';
import type {FlexLibraryItemEntityBase} from '../library-item';

export abstract class FlexLibraryEntityBase extends FlexTypeableEntity<FlexLibrary> implements FlexLibrary {
  @Column()
  name!: string;

  @OneToMany<FlexLibraryItemEntityBase>('FlexLibraryItemEntityBase', item => item.library, {
    cascade: true,
    // eager: true,
  })
  items!: FlexLibraryItemEntityBase[];

  @Column('simple-json')
  scanOptions!: FlexLibraryScanOptions;
}

@Entity()
@TableInheritance({column: {name: 'entityType', type: 'varchar'}})
export class FlexLibraryEntity extends FlexLibraryEntityBase {}
