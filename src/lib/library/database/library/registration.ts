import {FlexLibraryEntity} from './entity.class';
import {provideFlexDatabaseEntities} from '#/core/database';
import Container from 'typedi';
import {FLEX_LIBRARY_DATABASE_ENTITY} from './token';

provideFlexDatabaseEntities(FlexLibraryEntity);
Container.set(FLEX_LIBRARY_DATABASE_ENTITY, FlexLibraryEntity);
