import {Constructable} from 'typedi';
import type {FlexLibraryEntityBase} from './entity.class';

import {createScopedSingleToken} from '#/core/di';

export const {
  token: FLEX_LIBRARY_DATABASE_ENTITY,
  provide: provideFlexLibraryDatabaseEntityType,
  inject: injectFlexLibraryDatabaseEntity,
} = createScopedSingleToken<Constructable<FlexLibraryEntityBase>>('FLEX_LIBRARY_DATABASE_ENTITY');
