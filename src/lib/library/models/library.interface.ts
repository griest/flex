import {FlexIdentifiable, FlexTypeable} from '#/core';
import type {FlexLibraryItem} from './library-item.interface';
import type {FlexLibraryScanOptions} from './library-scan-options.interface';

export interface FlexLibrary<Item extends FlexLibraryItem = FlexLibraryItem> extends FlexIdentifiable, FlexTypeable {
  items: Item[];
  name: string;
  scanOptions: FlexLibraryScanOptions;
}
