import {FlexFSPath} from '#/fs';
import {FlexFSScanOptions} from '#/fs/server';

export interface FlexLibraryScanOptions {
  directories: FlexFSPath[];
  extensions: FlexFSScanOptions['extensions'];
  minSize: FlexFSScanOptions['minSize'];
}
