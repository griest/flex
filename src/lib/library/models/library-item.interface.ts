import {FlexIdentifiable, FlexTypeable} from '#/core';
import {FlexMediaItem, FlexMediaSource} from '#/media';
import {FlexMetadata} from '#/metadata';

/**
 * An item that can appear in a library.
 * It can optionally contain a media item which indicates whether or not it is a playable library item.
 * Unplayable library items are useful for parenting groups of other related library items with a distinct metadata.
 */
export interface FlexLibraryItem<
  Metadata extends FlexMetadata = FlexMetadata,
  MediaSource extends FlexMediaSource = FlexMediaSource,
> extends FlexIdentifiable, FlexTypeable {
  /**
   * The human-readable collection of metadata and info associated with this media item.
   */
  metadata?: Metadata;
  media?: FlexMediaItem<MediaSource>;
  name: string;
}
