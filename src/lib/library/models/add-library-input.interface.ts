import type {FlexLibraryScanOptions} from './library-scan-options.interface';
import type {FlexLibrary} from './library.interface';

export interface FlexAddLibraryInput {
  type: FlexLibrary['type'];
  name: FlexLibrary['name'];
  scanOptions: FlexLibraryScanOptions;
}
