import Container, {Token} from 'typedi';
import {FlexLibraryScanOptions} from '../models';

export const FLEX_LIBRARY_SCAN_OPTIONS = new Token<FlexLibraryScanOptions>('FLEX_LIBRARY_SCAN_OPTIONS');

export const provideFlexLibraryScanOptions = (libraryType: string, options: FlexLibraryScanOptions) => {
  Container.of(libraryType).set({
    id: FLEX_LIBRARY_SCAN_OPTIONS,
    value: options,
  });
};
