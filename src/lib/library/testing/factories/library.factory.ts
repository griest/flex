import {FlexLibrary} from '#/library';

export class MockFlexLibrary implements FlexLibrary {
  items = [];
  name = 'name';
  uid = 'uid';
  type = 'type';
  scanOptions = {
    directories: [],
    extensions: [],
    minSize: 0,
  };
}
