import {FLEX_LIBRARY_TYPES} from '../../tokens';
import Container from 'typedi';

export const flexLibraryValidateRegisteredType = (type: string) => {
  if (!Container.getMany(FLEX_LIBRARY_TYPES).includes(type)) {
    throw new Error(`${type} is not a registered library type. Did you call \`provideFlexLibraryType('${type}')\`?`);
  }
};
