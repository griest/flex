import {FlexFSFile} from '#/fs';
import {FlexMediaSource} from '#/media';

export interface FlexLibraryFileDiff<TMediaSource extends FlexMediaSource = FlexMediaSource> {
  /**
   * - scanned sources that are already in the library
   */
  existing: TMediaSource[];
  /**
   * - sources in the library that are not in the scan
   */
  stale: TMediaSource[];
  /**
   * - scanned files that are not yet in the library
   */
  newFiles: FlexFSFile[];
}
