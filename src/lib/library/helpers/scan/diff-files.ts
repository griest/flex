import {FlexFSFile} from '#/fs';
import {FlexMediaSource} from '#/media';
import {differenceWith, isEqual, keyBy, map} from 'lodash';
import {FlexLibraryFileDiff} from './file-diff.type';

/**
 * Separates files into three categories based on their presence in the library and the list of files.
 */
export const flexLibraryDiffFiles = <TMediaSource extends FlexMediaSource = FlexMediaSource>(sources: TMediaSource[], files: FlexFSFile[]): FlexLibraryFileDiff<TMediaSource> => {
  const allSources = keyBy(sources, 'uid');
  const allFiles = map(allSources, ({file, uid}) => ({uid, file}));
  const newFiles = differenceWith(files, allFiles, (f1, f2) => isEqual(f1.path, f2.file.path));
  const existingFiles = differenceWith(allFiles, files, (f1, f2) => isEqual(f1.file.path, f2.path));
  const staleFiles = differenceWith(allFiles, existingFiles, (f1, f2) => isEqual(f1.uid, f2.uid));

  return {
    newFiles,
    existing: existingFiles.map(({uid}) => allSources[uid]),
    stale: staleFiles.map(({uid}) => allSources[uid]),
  };
};
