
import {getTypeORMStoreOptions} from '#/core/state/pinia';
import {defineStore} from 'pinia';
import {FlexLibraryEntity, FlexLibraryItemEntity} from '../database';
import {injectFlexLibrariesDriver} from '../driver';
import {FlexLibrary, FlexLibraryItem} from '../models';
import {FLEX_LIBRARY_STATE_KEY} from './constants';

export const useFlexLibraryItemEntityStore = defineStore(`${FLEX_LIBRARY_STATE_KEY}.libraryItemEntities`, getTypeORMStoreOptions<FlexLibraryItem>(FlexLibraryItemEntity));
export const useFlexLibraryEntityStore = defineStore(`${FLEX_LIBRARY_STATE_KEY}.libraryEntities`, getTypeORMStoreOptions<FlexLibrary>(FlexLibraryEntity));
export const useFlexLibraryStore = defineStore(FLEX_LIBRARY_STATE_KEY, {
  actions: {
    async fetch () {
      const driver = injectFlexLibrariesDriver();
      const entityStore = useFlexLibraryEntityStore();
      const libraries = await driver.list();
      return (entityStore as any).set(...libraries);
    },
  },
});
