import {provideFlexLibrariesDriver} from '#/library/driver';
import {FlexLibraryRestDriverService} from './driver.class';

provideFlexLibrariesDriver(FlexLibraryRestDriverService);
