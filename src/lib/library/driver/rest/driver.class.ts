import {FlexLibrariesDriverInterface} from '#/library/driver';
import {Service} from 'typedi';
import {Axios} from 'axios';
import {FlexLibrary} from '#/library';

@Service()
export class FlexLibraryRestDriverService implements FlexLibrariesDriverInterface {
  constructor (
    private client: Axios,
  ) {}

  async list (): Promise<FlexLibrary[]> {
    const resp = await this.client.get<FlexLibrary[]>('/libraries');

    return resp.data;
  }
}
