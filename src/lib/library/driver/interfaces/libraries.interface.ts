import {FlexLibrary} from '#/library';

export interface FlexLibrariesDriverInterface {
  /**
   * Gets a list of libraries.
   */
  list(): Promise<FlexLibrary[]>;
}
