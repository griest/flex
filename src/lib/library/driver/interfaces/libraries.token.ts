import Container, {Constructable, Token} from 'typedi';
import {FlexLibrariesDriverInterface} from './libraries.interface';

export const FLEX_LIBRARIES_DRIVER = new Token<FlexLibrariesDriverInterface>('FLEX_LIBRARIES_DRIVER');

export const provideFlexLibrariesDriver = (driver: Constructable<FlexLibrariesDriverInterface>) => {
  Container.set({
    id: FLEX_LIBRARIES_DRIVER,
    type: driver,
  });
};

export const injectFlexLibrariesDriver = () => Container.get(FLEX_LIBRARIES_DRIVER);
