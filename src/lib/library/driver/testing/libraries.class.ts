import {Service} from 'typedi';
import {FlexLibrariesDriverInterface} from '../interfaces/libraries.interface';
import {MockFlexLibrary} from '#/library/testing';

@Service()
export class FlexLibrariesTestingDriver implements FlexLibrariesDriverInterface {
  async list () {
    return [
      new MockFlexLibrary(),
    ];
  }
}
