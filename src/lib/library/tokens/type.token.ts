import Container, {Token} from 'typedi';

export const FLEX_LIBRARY_TYPES = new Token<string>('FLEX_LIBRARY_TYPES');

export const provideFlexLibraryType = (type: string) => {
  Container.set({
    id: FLEX_LIBRARY_TYPES,
    value: type,
    multiple: true,
  });
};
