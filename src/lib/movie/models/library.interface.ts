import {FlexLibrary} from '#/library';
import {FLEX_MOVIE_TYPE} from '../constants';
import {FlexMovieLibraryItem} from './library-item.interface';

export interface FlexMovieLibrary extends FlexLibrary<FlexMovieLibraryItem> {
  type: typeof FLEX_MOVIE_TYPE;
}
