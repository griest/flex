import type {FlexVideoMetadata} from '#/video';
import {FLEX_MOVIE_TYPE} from '../constants';

export interface FlexMovieMetadata extends FlexVideoMetadata {
  type: typeof FLEX_MOVIE_TYPE;
}
