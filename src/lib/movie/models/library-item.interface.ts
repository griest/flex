import type {FlexLibraryItem} from '#/library';
import type {FlexVideoMediaSource} from '#/video';
import {FLEX_MOVIE_TYPE} from '../constants';
import type {FlexMovieMetadata} from './metadata.interface';

export interface FlexMovieLibraryItem extends FlexLibraryItem<FlexMovieMetadata, FlexVideoMediaSource> {
  type: typeof FLEX_MOVIE_TYPE;
}
