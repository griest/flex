import {FlexMovieEntity, FlexMovieMetadataEntity} from './entity.class';
import {provideFlexDatabaseEntities} from '#/core/database';

provideFlexDatabaseEntities(
  FlexMovieEntity,
  FlexMovieMetadataEntity,
);
