import {FlexLibraryItemEntity} from '#/library/database';
import {FLEX_MOVIE_TYPE, FlexMovieLibraryItem, FlexMovieMetadata} from '#/movie';
import {FlexVideoMediaItemEntity, FlexVideoMetadataEntity} from '#/video/database';
import {ChildEntity, Column} from 'typeorm';

@ChildEntity()
export class FlexMovieMetadataEntity extends FlexVideoMetadataEntity implements FlexMovieMetadata {
  @Column({
    default: FLEX_MOVIE_TYPE,
  })
  type: typeof FLEX_MOVIE_TYPE = FLEX_MOVIE_TYPE;
}

@ChildEntity()
export class FlexMovieEntity extends FlexLibraryItemEntity implements FlexMovieLibraryItem {
  @Column({
    default: FLEX_MOVIE_TYPE,
  })
  type: typeof FLEX_MOVIE_TYPE = FLEX_MOVIE_TYPE;;

  declare media?: FlexVideoMediaItemEntity;
  declare metadata?: FlexMovieMetadata;
}
