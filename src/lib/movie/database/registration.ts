import {provideLibraryScanIngress} from '#/library/database';
import {FLEX_MOVIE_TYPE} from '../constants';
import '#/library/database/registration';
import '#/video/database/registration';
import './item/registration';
import './library/registration';
import {flexMovieDatabaseScanIngress} from './scan';

provideLibraryScanIngress(FLEX_MOVIE_TYPE, flexMovieDatabaseScanIngress);
