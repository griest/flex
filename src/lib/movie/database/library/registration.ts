import {FlexMovieLibraryEntity} from './entity.class';
import {provideFlexDatabaseEntities} from '#/core/database';
import {provideFlexLibraryDatabaseEntityType} from '#/library/database';
import {FLEX_MOVIE_TYPE} from '#/movie/constants';

provideFlexDatabaseEntities(
  FlexMovieLibraryEntity,
);
provideFlexLibraryDatabaseEntityType(FLEX_MOVIE_TYPE, FlexMovieLibraryEntity);
