import {FlexLibraryEntity} from '#/library/database';
import {FLEX_MOVIE_TYPE, FlexMovieLibrary} from '#/movie';
import {ChildEntity, Column} from 'typeorm';
import {FlexMovieEntity} from '../item';

@ChildEntity()
export class FlexMovieLibraryEntity extends FlexLibraryEntity implements FlexMovieLibrary {
  @Column({
    default: FLEX_MOVIE_TYPE,
  })
  type: typeof FLEX_MOVIE_TYPE = FLEX_MOVIE_TYPE;;

  declare items: FlexMovieEntity[];
}
