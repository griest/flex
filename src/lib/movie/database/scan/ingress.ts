import {flexLibraryDiffFiles} from '#/library';
import {FlexLibraryScanIngress} from '#/library/database';
import {FlexMetadataPerson, FLEX_METADATA_SEARCH_QUERY_SANITIZERS} from '#/metadata';
import {FLEX_METADATA_DRIVER, injectFlexMetadataDriver} from '#/metadata/driver';
import {FLEX_DATABASE_SOURCE} from '#/core/database';
import {awaitValues} from '#/util';
import {FlexMetadataCastCreditEntity, FlexMetadataCrewCreditEntity, FlexVideoMediaSourceEntity} from '#/video/database';
import {compact, groupBy, map, mapValues, orderBy} from 'lodash';
import Container from 'typedi';
import {FlexMovieEntity, FlexMovieMetadataEntity} from '../item';
import {FlexMovieMetadata} from '#/movie';
import {FlexMovieLibraryEntity} from '../library';
import {FlexMovieMetadataDriverInterface} from '#/movie/driver';
import {FlexMetadataPersonEntity} from '#/metadata/database';

// const collectPeople = (library: FlexMovieLibraryEntity, metadataResponse)

export const flexMovieDatabaseScanIngress: FlexLibraryScanIngress = async (libraryId, files) => {
  const connection = Container.get(FLEX_DATABASE_SOURCE);
  const sourceRepository = connection.getRepository(FlexVideoMediaSourceEntity);
  const libraryRepo = connection.getRepository(FlexMovieLibraryEntity);
  const itemRepo = connection.getRepository(FlexMovieEntity);
  const metadataRepo = connection.getRepository(FlexMovieMetadataEntity);
  const personRepo = connection.getRepository(FlexMetadataPersonEntity);
  const library = await libraryRepo.findOneOrFail({
    where: {
      uid: libraryId,
    },
    relations: ['items', 'items.media', 'items.metadata.cast', 'items.metadata.crew'],
  });
  const container = Container.of(library.type);
  const driver = injectFlexMetadataDriver<FlexMovieMetadataDriverInterface>(library.type);

  const librarySources = compact(library.items?.flatMap(({media}) => media?.sources));
  const {
    newFiles,
    // existing,
    // stale,
  } = flexLibraryDiffFiles(librarySources, files);
  const sanitize = (str: string) =>
    container.getMany(FLEX_METADATA_SEARCH_QUERY_SANITIZERS).reduce((name, sanitize) => sanitize(name), str);

  const sanitized = groupBy(newFiles, ({name}) => sanitize(name));

  sanitized['']?.forEach(file => console.log(`A sanitized name could not be generated for file: ${file.name}`));
  delete sanitized[''];

  // refinement
  const resultsMap = await awaitValues<FlexMovieMetadata | undefined>(mapValues(sanitized, async (val, name) => {
    const results = await driver.search(name);
    if (results.length > 0) {
      const result = orderBy(results, 'score', 'desc')[0];
      const metadataResponse = await driver.fetch(result.uid);
      console.log(`Found metadata for movie: ${name}`);
      return metadataResponse;
    } else {
      console.log(`Cannot find any metadata for movie: ${name}`);
    }
  }));

  await Promise.all(map(resultsMap, async (metadataResponse, sanitizedName) => {
    const movie = library.items.find(({metadata, name}) =>
      metadataResponse
        ? metadata && metadata.uid === metadataResponse?.uid
        : name === sanitizedName,
    );

    // const people = ((await personRepo.upsert(
    //   map([
    //     ...(metadataResponse?.cast ?? []),
    //     ...(metadataResponse?.crew ?? []),
    //   ], 'person'),
    //   {
    //     conflictPaths: ['uid'],
    //     skipUpdateIfNoValuesChanged: true, // supported by postgres, skips update if it would not change row values
    //   },
    // )).generatedMaps as FlexMetadataPerson[]).reduce((acc, person) => {
    //   acc[person.uid] = person
    // });

    if (movie) {
      movie.media?.sources.push(...sanitized[sanitizedName].map(file =>
        sourceRepository.create({file, type: library.type}),
      ));
      movie.metadata = metadataResponse;
    } else {
      const items = itemRepo.create({
        type: library.type,
        name: sanitizedName,
        metadata: metadataResponse,
        media: {
          sources: sanitized[sanitizedName].map(file => ({file, type: library.type})),
        },
      });
      library.items.push(items);
    }
  }));

  libraryRepo.save(library);

  return library;
};
