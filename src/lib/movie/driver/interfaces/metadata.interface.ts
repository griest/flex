import type {FlexMetadataDriverInterface} from '#/metadata/driver';
import type {FlexMovieMetadata} from '#/movie';

export type FlexMovieMetadataDriverInterface = FlexMetadataDriverInterface<FlexMovieMetadata>;
