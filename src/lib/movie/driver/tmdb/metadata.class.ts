import type {FlexMetadataAgentSearchResult} from '#/metadata';
import {MovieDb} from 'moviedb-promise';
import {tmdbMovieResponseTransform, tmdbMovieSearchResultTransform} from './transforms';
import {Service} from 'typedi';
import {FlexMovieMetadata} from '#/movie';
import {FlexMovieMetadataDriverInterface} from '#/movie/driver';
import type {TmdbMovieDriverGetMovieResponse} from './models';

@Service()
export class TmdbMovieMetadataDriverService implements FlexMovieMetadataDriverInterface {
  constructor (
    private client: MovieDb,
  ) {}

  async search (query: string): Promise<FlexMetadataAgentSearchResult[]> {
    const resp = await this.client.searchMovie({
      query,
    });

    return resp.results?.map(tmdbMovieSearchResultTransform) || [];
  }

  async fetch (resultId: string): Promise<FlexMovieMetadata> {
    const resp = await this.client.movieInfo({
      id: resultId,
      append_to_response: 'credits',
    }) as TmdbMovieDriverGetMovieResponse;

    return tmdbMovieResponseTransform(resp, 'https://www.themoviedb.org/');
  }
}
