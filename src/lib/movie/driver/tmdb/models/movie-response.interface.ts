import type {CreditsResponse, MovieResponse} from 'moviedb-promise';

export interface TmdbMovieDriverGetMovieResponse extends MovieResponse {
  credits: CreditsResponse;
}
