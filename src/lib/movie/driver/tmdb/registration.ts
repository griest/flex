import {provideFlexMetadataDriver} from '#/metadata/driver';
import {FLEX_MOVIE_TYPE} from '#/movie';
import {TmdbMovieMetadataDriverService} from './metadata.class';

provideFlexMetadataDriver(FLEX_MOVIE_TYPE, TmdbMovieMetadataDriverService);
