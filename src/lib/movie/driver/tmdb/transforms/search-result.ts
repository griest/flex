import {FlexMetadataAgentSearchResult, FlexMetadataStatus} from '#/metadata';
import {FLEX_MOVIE_TYPE} from '#/movie';
import type {MovieResult} from 'moviedb-promise';

export const tmdbMovieSearchResultTransform = (result: MovieResult): FlexMetadataAgentSearchResult => ({
  status: FlexMetadataStatus.UNREFINED,
  type: FLEX_MOVIE_TYPE,
  uid: result.id?.toString() || '',
  year: result.release_date ? new Date(result.release_date).getUTCFullYear().toString() : '',
  title: result.title || '',
  score: result.popularity || 0,
});
