import {FLEX_MOVIE_TYPE} from '#/movie';
import type {FlexMovieMetadata} from '#/movie';
import {tmdbCrewTransform, TMDB_ID_PREFIX, tmdbCastTransform} from '#/driver/tmdb';
import {FlexPlayableLengthUnit} from '#/player';
import {FlexMetadataStatus} from '#/metadata';
import type {TmdbMovieDriverGetMovieResponse} from '../models';

export const tmdbMovieResponseTransform = (result: TmdbMovieDriverGetMovieResponse, baseUrl: string): FlexMovieMetadata => ({
  status: FlexMetadataStatus.REFINED,
  uid: `${TMDB_ID_PREFIX}${result.id}`,
  type: FLEX_MOVIE_TYPE,
  year: result.release_date ? new Date(result.release_date).getUTCFullYear().toString() : '',
  title: result.title || '',
  description: result.overview,
  image: result.poster_path && `${baseUrl}t/p/original${result.poster_path}`,
  thumbnail: result.poster_path && `${baseUrl}t/p/w220_and_h330_face${result.poster_path}`,
  backdrop: result.backdrop_path && `${baseUrl}t/p/original${result.backdrop_path}`,
  runtime: result.runtime
    ? {
      length: result.runtime,
      units: FlexPlayableLengthUnit.MINUTES,
    }
    : undefined,
  cast: result.credits.cast?.map(tmdbCastTransform) || [],
  crew: result.credits.crew?.map(tmdbCrewTransform) || [],
  credits: [],
});
