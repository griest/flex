import {FLEX_AUDIO_METADATA_SEARCH_QUERY_SANITIZER_TERMS} from '#/audio';
import {provideFlexLibraryScanOptions, provideFlexLibraryType} from '#/library';
import {flexMetadataSearchQuerySeparatorSanitizer, flexMetadataSearchQueryYearSanitizer, FLEX_METADATA_SEARCH_QUERY_SANITIZER_TERMS, provideFlexMetadataSearchQuerySanitizers, provideFlexMetadataSearchQueryTermRemoverSanitizers} from '#/metadata';
import {FLEX_VIDEO_METADATA_SEARCH_QUERY_SANITIZER_TERMS} from '#/video';
import {FLEX_MOVIE_TYPE} from './constants';

provideFlexLibraryType(FLEX_MOVIE_TYPE);
provideFlexMetadataSearchQueryTermRemoverSanitizers(
  FLEX_MOVIE_TYPE,
  ...FLEX_METADATA_SEARCH_QUERY_SANITIZER_TERMS,
  ...FLEX_AUDIO_METADATA_SEARCH_QUERY_SANITIZER_TERMS,
  ...FLEX_VIDEO_METADATA_SEARCH_QUERY_SANITIZER_TERMS,
);
provideFlexMetadataSearchQuerySanitizers(
  FLEX_MOVIE_TYPE,
  flexMetadataSearchQuerySeparatorSanitizer,
  flexMetadataSearchQueryYearSanitizer,
);
provideFlexLibraryScanOptions(FLEX_MOVIE_TYPE, {
  directories: [],
  extensions: ['.mkv', '.mp4'],
  minSize: 4e8,
});
