import FlexMovieGraphQlSchema from './schema.gqlschema';
import {provideApiGraphQlSchema} from '#/api/graphql';
import '#/library/api/graphql/registration';
import '#/video/api/graphql/registration';
import {provideLibraryApiGraphQlItemType, provideLibraryApiGraphQlType} from '#/library/api/graphql';
import {FLEX_MOVIE_TYPE} from '#/movie/constants';

provideApiGraphQlSchema(FlexMovieGraphQlSchema);
provideLibraryApiGraphQlType(FLEX_MOVIE_TYPE, 'MovieLibrary');
provideLibraryApiGraphQlItemType(FLEX_MOVIE_TYPE, 'MovieLibraryItem');
