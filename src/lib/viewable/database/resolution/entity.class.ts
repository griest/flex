import {FlexViewableResolution} from '#/viewable';
import {Column} from 'typeorm';

export class FlexViewableResolutionEntity implements FlexViewableResolution {
  @Column()
  width!: number;

  @Column()
  height!: number;
}
