import {FlexMediaSource} from '#/media';
import {FlexViewableResolution} from './resolution.interface';

export interface FlexViewableMediaSource extends FlexMediaSource {
  resolution: FlexViewableResolution;
}
