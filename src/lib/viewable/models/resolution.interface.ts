export interface FlexViewableResolution {
  /**
   * The width in pixels.
   */
  width: number;
  /**
   * The height in pixels.
   */
  height: number;
}
