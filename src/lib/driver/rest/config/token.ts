import {createSingleToken} from '#/core/di';
import {FlexDriverRestClientConfig} from './interface';

export const {
  token: FLEX_DRIVER_REST_CLIENT_CONFIG,
  provide: provideDriverRestClientConfig,
  inject: injectDriverRestClientConfig,
} = createSingleToken<FlexDriverRestClientConfig>('FLEX_DRIVER_REST_CLIENT_CONFIG');
