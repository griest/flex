import {Axios} from 'axios';
import Container from 'typedi';
import {injectDriverRestClientConfig} from './config';

Container.set({
  id: Axios,
  factory: () => {
    const baseUrl = injectDriverRestClientConfig().baseUrl;
    return new Axios({
      baseURL: baseUrl,
      // I shouldn't need to do this
      transformResponse: data => JSON.parse(data),
    });
  },
  global: true,
});
