import {FlexMetadataCastCredit, FLEX_VIDEO_METADATA_CAST_KIND} from '#/video';
import type {Cast, Person} from 'moviedb-promise';
import {TMDB_ID_PREFIX} from '../constants';
import {tmdbPersonTransform} from './person';

export const tmdbCastTransform = (cast: Cast): FlexMetadataCastCredit => ({
  uid: `${TMDB_ID_PREFIX}${cast.credit_id}`,
  order: cast.order || 99,
  kind: FLEX_VIDEO_METADATA_CAST_KIND,
  person: tmdbPersonTransform(<Person>cast),
  character: cast.character || '',
});
