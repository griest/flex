import {FlexMetadataCrewCredit, FLEX_VIDEO_METADATA_CREW_KIND} from '#/video';
import type {Crew, Person} from 'moviedb-promise';
import {TMDB_ID_PREFIX} from '../constants';
import {tmdbPersonTransform} from './person';

export const tmdbCrewTransform = (person: Crew): FlexMetadataCrewCredit => ({
  uid: `${TMDB_ID_PREFIX}${person.credit_id}`,
  order: 99,
  kind: FLEX_VIDEO_METADATA_CREW_KIND,
  person: tmdbPersonTransform(<Person>person),
  job: person.job || '',
});
