import type {FlexMetadataPerson} from '#/metadata';
import type {Person} from 'moviedb-promise';
import {TMDB_ID_PREFIX} from '../constants';

export const tmdbPersonTransform = (person: Person): FlexMetadataPerson => ({
  uid: `${TMDB_ID_PREFIX}${person.id}`,
  name: person.name || '',
  primaryRole: person.known_for_department || '',
  description: person.biography || '',
  headshot: `/t/p/w600_and_h900_bestv2${person.profile_path}`,
});
