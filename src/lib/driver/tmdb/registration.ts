import {MovieDb} from 'moviedb-promise';
import Container from 'typedi';
import {injectDriverTmdbClientConfig} from './config';

Container.set({
  id: MovieDb,
  factory: () => {
    const {
      apiKey,
      baseUrl,
    } = injectDriverTmdbClientConfig();
    return new MovieDb(apiKey, baseUrl);
  },
  global: true,
});
