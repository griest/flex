import {createSingleToken} from '#/core/di';
import {FlexDriverTmdbClientConfig} from './interface';

export const {
  token: FLEX_DRIVER_TMDB_CLIENT_CONFIG,
  provide: provideDriverTmdbClientConfig,
  inject: injectDriverTmdbClientConfig,
} = createSingleToken<FlexDriverTmdbClientConfig>('FLEX_DRIVER_TMDB_CLIENT_CONFIG');
