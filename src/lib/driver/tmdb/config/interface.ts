export interface FlexDriverTmdbClientConfig {
  apiKey: string;
  baseUrl: string;
}
