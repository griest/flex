import {FlexMetadata, FlexMetadataAgentSearchResult} from '#/metadata';

export interface FlexMetadataDriverInterface<Metadata extends FlexMetadata = FlexMetadata> {
  /**
   * Search for metadata for a particular library item.
   * @param query
   */
  search(query: string): Promise<FlexMetadataAgentSearchResult[]>;
  /**
   * Fetch the metadata for a particular search result.
   * @param result
   */
  fetch(resultId: FlexMetadataAgentSearchResult['uid']): Promise<Metadata>;
}
