import Container, {Constructable, Token} from 'typedi';
import {FlexMetadataDriverInterface} from './metadata.interface';

export const FLEX_METADATA_DRIVER = new Token<FlexMetadataDriverInterface>('FLEX_METADATA_DRIVER');

export const provideFlexMetadataDriver = (libraryType: string, driver: Constructable<FlexMetadataDriverInterface>) => {
  Container.of(libraryType).set({
    id: FLEX_METADATA_DRIVER,
    type: driver,
  });
};

export const injectFlexMetadataDriver = <T extends FlexMetadataDriverInterface = FlexMetadataDriverInterface>(libraryType: string): T => {
  return Container.of(libraryType).get<T>(FLEX_METADATA_DRIVER);
};
