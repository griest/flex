import FlexMetadataGraphQlSchema from './schema.gqlschema';
import '#/api/graphql/registration';
import {provideApiGraphQlSchema} from '#/api/graphql';

provideApiGraphQlSchema(FlexMetadataGraphQlSchema);
