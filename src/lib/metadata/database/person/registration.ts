import {FlexMetadataPersonEntity} from './entity.class';
import {provideFlexDatabaseEntities} from '#/core/database';

provideFlexDatabaseEntities(FlexMetadataPersonEntity);
