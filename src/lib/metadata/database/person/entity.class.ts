import type {FlexMetadataPerson} from '#/metadata';
import {Column, Entity, OneToMany, PrimaryColumn, TableInheritance} from 'typeorm';
import type {FlexMetadataCreditEntity} from '../credit';

@Entity()
@TableInheritance({column: {name: 'entityType', type: 'varchar'}})
export class FlexMetadataPersonEntity implements FlexMetadataPerson {
  @PrimaryColumn()
  uid!: string;

  @Column()
  name!: string;

  @Column()
  primaryRole!: string;;

  @Column({
    type: 'text',
    nullable: true,
  })
  description?: string;

  @Column({
    type: 'text',
    nullable: true,
  })
  headshot?: string;

  @OneToMany<FlexMetadataCreditEntity>('FlexMetadataCreditEntity', credit => credit.person)
  credits!: FlexMetadataCreditEntity[];
}
