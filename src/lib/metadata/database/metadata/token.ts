import {Constructable, Token} from 'typedi';
import type {FlexMetadataEntity} from './entity.class';

export interface FlexMetadataDatabaseEntityTypeRegistration<
  Entity extends FlexMetadataEntity = FlexMetadataEntity,
> {
  type: string;
  entity?: Constructable<Entity>;
}

export const FLEX_METADATA_DATABASE_ENTITY_REGISTRATIONS = new Token<FlexMetadataDatabaseEntityTypeRegistration>('FLEX_METADATA_DATABASE_ENTITY_REGISTRATIONS');
