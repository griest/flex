import {FlexMetadataEntity} from './entity.class';
import {FLEX_TYPE_DEFAULT} from '#/core';
import Container from 'typedi';
import {provideFlexDatabaseEntities} from '#/core/database';
import {FlexMetadataDatabaseEntityTypeRegistration, FLEX_METADATA_DATABASE_ENTITY_REGISTRATIONS} from './token';
import '#/player/database/length/registration';

export const flexMetadataDatabaseRegistration: FlexMetadataDatabaseEntityTypeRegistration<FlexMetadataEntity> = {
  type: FLEX_TYPE_DEFAULT,
  entity: FlexMetadataEntity,
};

Container.set<typeof flexMetadataDatabaseRegistration>({
  id: FLEX_METADATA_DATABASE_ENTITY_REGISTRATIONS,
  value: flexMetadataDatabaseRegistration,
  multiple: true,
});
provideFlexDatabaseEntities(FlexMetadataEntity);
