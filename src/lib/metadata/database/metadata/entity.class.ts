import {FlexMetadata, FlexMetadataCredit, FlexMetadataStatus} from '#/metadata';
import {FlexPlayableLengthEntity} from '#/player/database';
import {Column, Entity, OneToMany, PrimaryColumn, TableInheritance} from 'typeorm';
import type {FlexMetadataCreditEntity} from '../credit';

@Entity()
@TableInheritance({column: {name: 'entityType', type: 'varchar'}})
export class FlexMetadataEntity implements FlexMetadata {
  @PrimaryColumn()
  uid!: string;

  @Column({
    type: 'text',
    default: FlexMetadataStatus.UNREFINED,
  })
  status!: FlexMetadataStatus;

  @Column()
  type!: string;

  @Column()
  title!: string;

  @Column({
    type: 'text',
    nullable: true,
  })
  description?: string;

  @Column()
  year!: string;

  @Column({
    type: 'text',
    nullable: true,
  })
  image?: string | null;

  @Column({
    type: 'text',
    nullable: true,
  })
  thumbnail?: string | null;

  @Column({
    type: 'text',
    nullable: true,
  })
  backdrop?: string | null;

  @Column(() => FlexPlayableLengthEntity)
  runtime?: FlexPlayableLengthEntity;

  @OneToMany<FlexMetadataCreditEntity>('FlexMetadataCreditEntity', credit => credit.item, {cascade: true})
  credits?: FlexMetadataCredit[];
}
