import {FlexMetadataCreditEntity} from './entity.class';
import {provideFlexDatabaseEntities} from '#/core/database';

provideFlexDatabaseEntities(FlexMetadataCreditEntity);
