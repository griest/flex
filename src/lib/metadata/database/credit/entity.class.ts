import type {FlexMetadata, FlexMetadataCredit} from '#/metadata';
import {Column, Entity, ManyToOne, PrimaryColumn, TableInheritance} from 'typeorm';
import type {FlexMetadataEntity} from '../metadata';
import type {FlexMetadataPersonEntity} from '../person';

@Entity()
@TableInheritance({column: {name: 'entityType', type: 'varchar'}})
export class FlexMetadataCreditEntity implements FlexMetadataCredit {
  @PrimaryColumn()
  uid!: string;

  @Column()
  kind!: string;

  @Column()
  order!: number;

  @ManyToOne<FlexMetadataPersonEntity>('FlexMetadataPersonEntity', person => person.credits, {cascade: true})
  person!: FlexMetadataPersonEntity;

  @ManyToOne<FlexMetadataEntity>('FlexMetadataEntity', itemMetadata => itemMetadata.credits)
  item!: FlexMetadata;
}
