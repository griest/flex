import {FlexMetadataStatus} from './status.enum';

export interface FlexMetadataRefinable {
  status: FlexMetadataStatus;
}
