import {FlexIdentifiable} from '#/core';
import type {FlexMetadataCredit} from './credit';

export interface FlexMetadataPerson extends FlexIdentifiable {
  name: string;
  primaryRole: string;
  description?: string;
  headshot?: string;
  credits?: FlexMetadataCredit[];
}
