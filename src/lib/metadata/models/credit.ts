import {FlexIdentifiable, FlexOrderable} from '#/core';
import type {FlexMetadata} from './metadata.interface';
import type {FlexMetadataPerson} from './person.interface';

export interface FlexMetadataCredit extends FlexOrderable, FlexIdentifiable {
  kind: string;
  person?: FlexMetadataPerson;
  item?: FlexMetadata;
}
