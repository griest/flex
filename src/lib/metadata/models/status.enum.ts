export enum FlexMetadataStatus {
  REFINED = 'refined',
  REFINING = 'refining',
  UNREFINED = 'unrefined',
}
