import {FlexIdentifiable, FlexTypeable} from '#/core';
import {FlexPlayableLength} from '#/player';
import type {FlexMetadataCredit} from './credit';
import type {FlexMetadataRefinable} from './refinable.interface';

export interface FlexMetadata extends FlexTypeable, FlexIdentifiable, FlexMetadataRefinable {
  title: string;
  description?: string;
  /**
   * The canonical year of release.
   */
  year: string;

  /**
   * The URL of the fullsize main image.
   */
  image?: string | null;

  /**
   * The URL of the smaller main image.
   */
  thumbnail?: string | null;

  /**
   * The URL of the backdrop banner image.
   */
  backdrop?: string | null;

  runtime?: FlexPlayableLength;

  credits?: FlexMetadataCredit[];
}
