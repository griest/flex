import {FlexIdentifiable} from '#/core';
import {FlexMetadata} from './metadata.interface';
import {FlexMetadataStatus} from './status.enum';

export interface FlexMetadataAgentSearchResult extends FlexMetadata, FlexIdentifiable {
  status: FlexMetadataStatus.UNREFINED;
  score: number;
}
