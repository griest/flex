import {FlexMetadataSearchQuerySanitizer} from './interface';

export const flexMetadataSearchQueryYearSanitizer: FlexMetadataSearchQuerySanitizer = (query: string) =>
  // this will break in 21xx
  query.replace(/19\d\d|20\d\d/, '').trim();
