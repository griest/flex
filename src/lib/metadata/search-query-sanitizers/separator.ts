import {FlexMetadataSearchQuerySanitizer} from './interface';

export const flexMetadataSearchQuerySeparatorSanitizer: FlexMetadataSearchQuerySanitizer = (query: string) =>
  query.replaceAll('.', ' ').trim();
