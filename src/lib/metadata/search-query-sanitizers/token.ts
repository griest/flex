import Container, {Token} from 'typedi';
import {flexMetadataCreateSearchQueryTermRemoverSanitizer} from './create-term-remover';
import type {FlexMetadataSearchQuerySanitizer} from './interface';

export const FLEX_METADATA_SEARCH_QUERY_SANITIZERS = new Token<FlexMetadataSearchQuerySanitizer>('FLEX_METADATA_SEARCH_QUERY_SANITIZERS');

export const provideFlexMetadataSearchQuerySanitizers = (libraryType: string, ...sanitizers: FlexMetadataSearchQuerySanitizer[]) => {
  sanitizers.forEach(sanitizer => {
    Container.of(libraryType).set({
      id: FLEX_METADATA_SEARCH_QUERY_SANITIZERS,
      value: sanitizer,
      multiple: true,
    });
  });
};

export const provideFlexMetadataSearchQueryTermRemoverSanitizers = (libraryType: string, ...terms: string[]) => {
  provideFlexMetadataSearchQuerySanitizers(libraryType, flexMetadataCreateSearchQueryTermRemoverSanitizer(...terms));
};
