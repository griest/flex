export type FlexMetadataSearchQuerySanitizer = (query: string) => string;
