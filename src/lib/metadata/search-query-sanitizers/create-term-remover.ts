import type {FlexMetadataSearchQuerySanitizer} from './interface';

export const flexMetadataCreateSearchQueryTermRemoverSanitizer =
  (...terms: string[]): FlexMetadataSearchQuerySanitizer =>
    (query: string) => query.replaceAll(new RegExp(
      terms.reduce((acc, term) => acc.concat(`${term}|`), ''),
      'g',
    ), '');
