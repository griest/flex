export interface ReversibleTransform<
  In extends Record<string, unknown> = Record<string, unknown>,
  Out extends Record<string, unknown> = Record<string, unknown>,
> {
  in(inObj: In, outObj: Out): In;
  out(inObj: In, outObj: Out): Out;
}
