import {ReversibleTransform} from '../models';

export class ReversibleMetaTransformer<
  In extends Record<string, unknown> = Record<string, unknown>,
  Out extends Record<string, unknown> = Record<string, unknown>,
> implements ReversibleTransform<In, Out> {
  constructor (
    private transforms: ReversibleTransform<In, Out>[],
  ) {}

  in (inObj: In, outObj: Out): In {
    return this.transforms.reduce((transformedIn, transform) => transform.in(transformedIn, outObj), inObj);
  }

  out (inObj: In, outObj: Out): Out {
    return this.transforms.reduce((transformedOut, transform) => transform.out(inObj, transformedOut), outObj);
  }
}
