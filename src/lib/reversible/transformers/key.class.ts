import {ReversibleTransform} from '../models';

export class ReversibleTransformer<
  InType,
  OutType = InType,
  InKey extends string = string,
  OutKey extends string = InKey,
> implements ReversibleTransform<
Record<InKey, InType>,
Record<OutKey, OutType>
> {
  public get outKey (): OutKey {
    return this._outKey || <OutKey><unknown> this.inKey;
  }

  constructor (
    public inKey: InKey,
    private _outKey?: OutKey,
    public inTransform: (outVal: OutType) => InType = e => <InType><unknown>e,
    public outTransform: (inVal: InType) => OutType = e => <OutType><unknown>e,
  ) {}

  in (inObj: Record<InKey, InType>, outObj: Record<OutKey, OutType>): Record<InKey, InType> {
    inObj[this.inKey] = this.inTransform(outObj[this.outKey]);

    return inObj;
  }

  out (inObj: Record<InKey, InType>, outObj: Record<OutKey, OutType>): Record<OutKey, OutType> {
    outObj[this.outKey] = this.outTransform(inObj[this.inKey]);

    return outObj;
  }
}
