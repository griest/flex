import {FlexFSFile} from '#/fs';

export const FLEX_TELEVISION_METADATA_SEASON_EPISODE_REGEX = /[Ss](?<season>\d+)[Ee](?<episode>\d+)/;

export interface FlexTelevisionMetadataSeasonEpisode {
  season?: number;
  episode?: number;
}

export const flexTelevisionMetedataGetSeasonAndEpisode = (file: FlexFSFile): FlexTelevisionMetadataSeasonEpisode => {
  const result = file.name.match(FLEX_TELEVISION_METADATA_SEASON_EPISODE_REGEX);
  return {
    season: Number(result?.groups?.season),
    episode: Number(result?.groups?.episode),
  };
};
