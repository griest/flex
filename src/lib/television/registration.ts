import {FLEX_AUDIO_METADATA_SEARCH_QUERY_SANITIZER_TERMS} from '#/audio';
import {provideFlexLibraryScanOptions, provideFlexLibraryType} from '#/library';
import {flexMetadataSearchQuerySeparatorSanitizer, flexMetadataSearchQueryYearSanitizer, FLEX_METADATA_SEARCH_QUERY_SANITIZER_TERMS, provideFlexMetadataSearchQuerySanitizers, provideFlexMetadataSearchQueryTermRemoverSanitizers} from '#/metadata';
import {FLEX_VIDEO_METADATA_SEARCH_QUERY_SANITIZER_TERMS} from '#/video';
import {FLEX_TELEVISION_TYPE} from './constants';

provideFlexLibraryType(FLEX_TELEVISION_TYPE);
provideFlexMetadataSearchQueryTermRemoverSanitizers(
  FLEX_TELEVISION_TYPE,
  ...FLEX_METADATA_SEARCH_QUERY_SANITIZER_TERMS,
  ...FLEX_AUDIO_METADATA_SEARCH_QUERY_SANITIZER_TERMS,
  ...FLEX_VIDEO_METADATA_SEARCH_QUERY_SANITIZER_TERMS,
);
provideFlexMetadataSearchQuerySanitizers(
  FLEX_TELEVISION_TYPE,
  flexMetadataSearchQuerySeparatorSanitizer,
  flexMetadataSearchQueryYearSanitizer,
);
provideFlexLibraryScanOptions(FLEX_TELEVISION_TYPE, {
  directories: [],
  extensions: ['.mkv', '.mp4', '.m4v'],
  minSize: 8e7,
});
