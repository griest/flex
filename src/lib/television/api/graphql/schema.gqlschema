enum TelevisionLibraryItemKind {
  SHOW
  SEASON
  EPISODE
}

interface TelevisionLibraryItem inherits LibraryItemInterface {
  kind: TelevisionLibraryItemKind!
}

interface TelevisionMetadata inherits VideoMetadataInterface {
  kind: TelevisionLibraryItemKind!
}

type TelevisionEpisodeMetadata implements TelevisionMetadata & VideoMetadataInterface inherits TelevisionMetadata {
  episodeNumber: Int!
  seasonNumber: Int!
}

type TelevisionEpisode implements TelevisionLibraryItem & LibraryItemInterface inherits TelevisionLibraryItem {
  metadata: TelevisionEpisodeMetadata
}

type TelevisionSeasonMetadata implements TelevisionMetadata & VideoMetadataInterface inherits TelevisionMetadata {
  episodeCount: Int!
  seasonNumber: Int!
}

type TelevisionSeason implements TelevisionLibraryItem & LibraryItemInterface inherits TelevisionLibraryItem {
  metadata: TelevisionSeasonMetadata
  episodes: [TelevisionEpisode!]!
}

type TelevisionShowMetadata implements TelevisionMetadata & VideoMetadataInterface inherits TelevisionMetadata {
  episodeCount: Int!
  seasonCount: Int!
}

type TelevisionShow implements TelevisionLibraryItem & LibraryItemInterface inherits TelevisionLibraryItem {
  metadata: TelevisionShowMetadata
  seasons: [TelevisionSeason!]!
}

type TelevisionLibrary implements LibraryInterface inherits LibraryInterface {
  type: String!
  items: [TelevisionLibraryItem!]!
  shows: [TelevisionShow!]!
}
