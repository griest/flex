import FlexTelevisionGraphQlSchema from './schema.gqlschema';
import {provideApiGraphQlSchema} from '#/api/graphql';
import '#/library/api/graphql/registration';
import '#/video/api/graphql/registration';
import {FLEX_TELEVISION_TYPE} from '#/television/constants';
import {provideLibraryApiGraphQlType, provideLibraryApiGraphQlItemType} from '#/library/api/graphql';

provideApiGraphQlSchema(FlexTelevisionGraphQlSchema);
provideLibraryApiGraphQlType(FLEX_TELEVISION_TYPE, 'MovieLibrary');
provideLibraryApiGraphQlItemType(FLEX_TELEVISION_TYPE, 'MovieLibraryItem');
