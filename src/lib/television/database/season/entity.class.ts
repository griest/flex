import {FlexLibraryItemEntity} from '#/library/database';
import {FlexTelevisionSeason, FlexTelevisionLibraryItemKind, FLEX_TELEVISION_TYPE, FlexTelevisionSeasonMetadata} from '#/television';
import {FlexVideoMetadataEntity} from '#/video/database';
import {ChildEntity, Column, ManyToOne, OneToMany} from 'typeorm';
import type {FlexTelevisionEpisodeEntity} from '../episode';
import type {FlexTelevisionShowEntity} from '../show';

@ChildEntity()
export class FlexTelevisionSeasonMetadataEntity extends FlexVideoMetadataEntity implements FlexTelevisionSeasonMetadata {
  kind: FlexTelevisionLibraryItemKind.SEASON = FlexTelevisionLibraryItemKind.SEASON;

  @Column({
    default: FLEX_TELEVISION_TYPE,
  })
  type: typeof FLEX_TELEVISION_TYPE = FLEX_TELEVISION_TYPE;;
  // item?: FlexVideoMediaItemEntity;

  @Column()
  episodeCount!: number;

  @Column()
  seasonNumber!: number;
}

@ChildEntity()
export class FlexTelevisionSeasonEntity extends FlexLibraryItemEntity implements FlexTelevisionSeason {
  kind: FlexTelevisionLibraryItemKind.SEASON = FlexTelevisionLibraryItemKind.SEASON;

  @Column({
    default: FLEX_TELEVISION_TYPE,
  })
  type: typeof FLEX_TELEVISION_TYPE = FLEX_TELEVISION_TYPE;;

  declare media: undefined;
  declare metadata: FlexTelevisionSeasonMetadataEntity;

  @OneToMany<FlexTelevisionEpisodeEntity>('FlexTelevisionEpisodeEntity', episode => episode.season, {
    cascade: true,
    // eager: true,
  })
  episodes!: FlexTelevisionEpisodeEntity[];

  @ManyToOne<FlexTelevisionShowEntity>('FlexTelevisionShowEntity', show => show.seasons)
  show!: FlexTelevisionShowEntity;
}
