import {FlexTelevisionSeasonEntity, FlexTelevisionSeasonMetadataEntity} from './entity.class';
import {provideFlexDatabaseEntities} from '#/core/database';

provideFlexDatabaseEntities(
  FlexTelevisionSeasonEntity,
  FlexTelevisionSeasonMetadataEntity,
);
