import {FlexFSFile, FlexFSPath} from '#/fs';
import {flexLibraryDiffFiles} from '#/library';
import {FlexLibraryScanIngress} from '#/library/database';
import {FlexMetadataSearchQuerySanitizer, FLEX_METADATA_SEARCH_QUERY_SANITIZERS} from '#/metadata';
import {FLEX_METADATA_DRIVER} from '#/metadata/driver';
import {injectFlexDatabaseSource} from '#/core/database';
import {FlexTelevisionMetadataDriverInterface} from '#/television/driver';
import {flexTelevisionMetedataGetSeasonAndEpisode} from '#/television/helpers';
import {FlexTelevisionEpisode, FlexTelevisionSeason, FlexTelevisionEpisodeMetadata, FlexTelevisionLibrary, FlexTelevisionSeasonMetadata} from '#/television';
import {awaitValues} from '#/util';
import {compact, each, groupBy, isEqual, map, mapValues, orderBy, partition} from 'lodash';
import Container from 'typedi';
import {FlexTelevisionLibraryEntity} from '../library';
import {FlexTelevisionSeasonEntity} from '../season';
import {FlexTelevisionShowEntity} from '../show';
import {DeepPartial} from 'typeorm';
import {FlexVideoMediaSource} from '#/video';
import {FlexTelevisionEpisodeEntity} from '../episode';
import {FlexVideoMediaSourceEntity} from '#/video/database';

interface ParsedFile {
  file: FlexFSFile;
  show: string;
  season?: number;
  episode?: number;
}

type ShowCollection = Record<string, Record<number, Record<number, ParsedFile[]>>>;

const sanitizeAndParseFiles = (sanitize: FlexMetadataSearchQuerySanitizer, files: FlexFSFile[], rootDirs: FlexFSPath[]): ParsedFile[] =>
  compact(files.map(file => {
  // call path.resolve for equality check?
    const rootDir = rootDirs.find(dir => isEqual(file.path.slice(0, dir.length), dir));
    if (rootDir) {
      const show = sanitize(file.path[rootDir.length]);
      const {
        season,
        episode,
      } = flexTelevisionMetedataGetSeasonAndEpisode(file);
      return {
        file,
        show,
        season,
        episode,
      };
    } else {
      console.log(`File ${file.name} is not under this library's scan directories; skipping.`);
    }

    return null;
  }));

const getParsedFileShowCollection = (parsedByShow: Record<string, ParsedFile[]>): ShowCollection =>
  mapValues(parsedByShow, (parsed, showName) => {
    const [knownFiles, unknownFiles] = partition(parsed, ({season, episode}) => Number.isFinite(season) && Number.isFinite(episode));
    if (unknownFiles.length > 0) {
      // TODO: deal with unknown
      console.log(`These sources in show ${showName} could not get season and episode info from the filename:`, ...unknownFiles.map(({file}) => file.name));
    }

    return mapValues(groupBy(knownFiles, 'season'), seasonFiles => groupBy(seasonFiles, 'episode'));
  });

const fetchMetadata = async (library: FlexTelevisionLibrary, parsedByShow: Record<string, ParsedFile[]>) => {
  const container = Container.of(library.type);
  const dataSource = injectFlexDatabaseSource();
  const showRepository = dataSource.getRepository(FlexTelevisionShowEntity);
  const seasonRepository = dataSource.getRepository(FlexTelevisionSeasonEntity);
  const episodeRepository = dataSource.getRepository(FlexTelevisionEpisodeEntity);
  const sourceRepository = dataSource.getRepository(FlexVideoMediaSourceEntity);
  const driver = container.get<FlexTelevisionMetadataDriverInterface>(FLEX_METADATA_DRIVER);

  const resultsByShow = await awaitValues(mapValues(parsedByShow, async (val, name) => {
    const results = await driver.search(name);
    if (results.length > 0) {
      const result = orderBy(results, 'score', 'desc')[0];
      const metadataResponse = await driver.fetch(result.uid);
      console.log(`Found metadata for show: ${name}`);
      return metadataResponse;
    } else {
      console.log(`Cannot find any metadata for show: ${name}`);
    }
  }));
  const showCollection = getParsedFileShowCollection(parsedByShow);
  return Promise.all(map(resultsByShow, async (metadataResponse, showName) => {
    const seasons = showCollection[showName];

    const show = library.shows.find(({metadata, name}) =>
      metadataResponse
        ? metadata && metadata.uid === metadataResponse?.uid
        : name === showName,
    );
    const itemsToAdd: (FlexTelevisionEpisodeEntity | FlexTelevisionSeasonEntity | FlexTelevisionShowEntity)[] = [];

    const addToItems = <T extends (FlexTelevisionEpisodeEntity | FlexTelevisionSeasonEntity | FlexTelevisionShowEntity) = (FlexTelevisionEpisodeEntity | FlexTelevisionSeasonEntity | FlexTelevisionShowEntity)>(item: T): T => {
      itemsToAdd.push(item);
      return item;
    };

    const createEpisode = (files: ParsedFile[], metadata?: FlexTelevisionEpisodeMetadata) =>
      episodeRepository.create({
        type: library.type,
        metadata: metadata,
        name: metadata?.title || '',
        media: {
          sources: files.map(({file}): DeepPartial<FlexVideoMediaSource> => ({
            type: library.type,
            file,
          })),
        },
      });

    const createSeason = (episodes: Record<number, ParsedFile[]>, metadata?: FlexTelevisionSeasonMetadata) =>
      seasonRepository.create({
        type: library.type,
        name: metadata?.title || '',
        metadata,
        episodes: map(episodes, (files, episodeNumber): DeepPartial<FlexTelevisionEpisode> =>
          addToItems(createEpisode(files, metadataResponse?.episodes[Number(metadata?.seasonNumber)][Number(episodeNumber)])),
        ),
      });

    const createShow = () =>
      showRepository.create({
        type: library.type,
        name: metadataResponse?.title || '',
        metadata: metadataResponse,
        seasons: map(seasons, (episodes, seasonNumber): DeepPartial<FlexTelevisionSeason> =>
          addToItems(createSeason(episodes, metadataResponse?.seasons[Number(seasonNumber)])),
        ),
      });

    if (show) {
      // TS, you stoopid
      const definitelyDefinedShow = show;
      show.metadata = metadataResponse;

      each(seasons, (episodes, seasonNumber) => {
        const seasonMetadata = metadataResponse?.seasons[Number(seasonNumber)];
        const existingSeason = definitelyDefinedShow.seasons.find(({metadata}) =>
          metadata && metadata.uid === seasonMetadata?.uid,
        );
        if (existingSeason) {
          existingSeason.metadata = seasonMetadata;

          each(episodes, (files, episodeNumber) => {
            const episodeMetadata = metadataResponse?.episodes[Number(seasonNumber)][Number(episodeNumber)];
            const existingEpisode = existingSeason.episodes.find(({metadata}) =>
              metadata && metadata.uid === episodeMetadata?.uid,
            );
            if (existingEpisode) {
              existingEpisode.metadata = episodeMetadata;
              existingEpisode.media?.sources.push(...files.map(({file}) => sourceRepository.create({
                type: library.type,
                file,
              })));
            } else {
              existingSeason.episodes.push(addToItems(createEpisode(files, episodeMetadata)));
            }
          });
        } else {
          definitelyDefinedShow.seasons.push(addToItems(createSeason(episodes, seasonMetadata)));
        }
      });
    } else {
      library.shows.push(createShow());
    }

    return itemsToAdd;
  }));
};

export const flexTelevisionDatabaseScanIngress: FlexLibraryScanIngress = async (libraryId, files) => {
  const dataSource = injectFlexDatabaseSource();
  const libraryRepo = dataSource.getRepository(FlexTelevisionLibraryEntity);
  const seasonRepository = dataSource.getRepository(FlexTelevisionSeasonEntity);
  const episodeRepository = dataSource.getRepository(FlexTelevisionEpisodeEntity);
  const library = await libraryRepo.findOneOrFail({
    where: {
      uid: libraryId,
    },
    relations: ['shows', 'items'],
  });
  const container = Container.of(library.type);

  const librarySources = compact(library.items?.flatMap(({media}) => media?.sources));
  const {
    newFiles,
    // existing,
    // stale,
  } = flexLibraryDiffFiles(librarySources, files);

  const sanitize = (str: string) =>
    container.getMany(FLEX_METADATA_SEARCH_QUERY_SANITIZERS).reduce((name, sanitize) => sanitize(name), str);
  const parsedFiles = sanitizeAndParseFiles(sanitize, newFiles, library.scanOptions.directories);

  const parsedByShow = groupBy(parsedFiles, 'show');
  parsedByShow['']?.forEach(({file}) => console.log(`A sanitized show name could not be generated for file: ${file.name}`));

  const newItems = (await fetchMetadata(library, parsedByShow)).flat().map(item => ({
    metadata: {
      uid: item.metadata?.uid,
    },
  }));

  await libraryRepo.save(library);

  const items = (await Promise.all([
    episodeRepository.findBy(newItems),
    seasonRepository.findBy(newItems),
  ])).flat();

  library.items.push(...items);
  await libraryRepo.save(library);

  return library;
};
