import {FlexTelevisionShowEntity, FlexTelevisionShowMetadataEntity} from './entity.class';
import {provideFlexDatabaseEntities} from '#/core/database';

provideFlexDatabaseEntities(
  FlexTelevisionShowEntity,
  FlexTelevisionShowMetadataEntity,
);
