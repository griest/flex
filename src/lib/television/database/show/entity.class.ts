import {FlexLibraryItemEntity} from '#/library/database';
import {FlexTelevisionShow, FlexTelevisionLibraryItemKind, FLEX_TELEVISION_TYPE, FlexTelevisionShowMetadata} from '#/television';
import {FlexVideoMetadataEntity} from '#/video/database';
import {ChildEntity, Column, OneToMany} from 'typeorm';
import type {FlexTelevisionSeasonEntity} from '../season';

@ChildEntity()
export class FlexTelevisionShowMetadataEntity extends FlexVideoMetadataEntity implements FlexTelevisionShowMetadata {
  kind: FlexTelevisionLibraryItemKind.SHOW = FlexTelevisionLibraryItemKind.SHOW;

  @Column({
    default: FLEX_TELEVISION_TYPE,
  })
  type: typeof FLEX_TELEVISION_TYPE = FLEX_TELEVISION_TYPE;;
  // item?: FlexVideoMediaItemEntity;

  @Column()
  episodeCount!: number;

  @Column()
  seasonCount!: number;
}

@ChildEntity()
export class FlexTelevisionShowEntity extends FlexLibraryItemEntity implements FlexTelevisionShow {
  kind: FlexTelevisionLibraryItemKind.SHOW = FlexTelevisionLibraryItemKind.SHOW;

  @Column({
    default: FLEX_TELEVISION_TYPE,
  })
  type: typeof FLEX_TELEVISION_TYPE = FLEX_TELEVISION_TYPE;;

  declare media: undefined;
  declare metadata: FlexTelevisionShowMetadataEntity;

  @OneToMany<FlexTelevisionSeasonEntity>('FlexTelevisionSeasonEntity', season => season.show, {
    cascade: true,
    // eager: true,
  })
  seasons!: FlexTelevisionSeasonEntity[];

  // @ManyToOne<FlexTelevisionLibraryEntity>('FlexTelevisionLibraryEntity', library => library.shows)
  // libraryTest!: FlexTelevisionLibraryEntity
}
