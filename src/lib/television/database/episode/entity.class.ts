import {FlexLibraryItemEntity} from '#/library/database';
import {FlexTelevisionEpisode, FlexTelevisionEpisodeMetadata, FlexTelevisionLibraryItemKind, FLEX_TELEVISION_TYPE} from '#/television';
import {FlexVideoMediaItemEntity, FlexVideoMetadataEntity} from '#/video/database';
import {ChildEntity, Column, ManyToOne} from 'typeorm';
import type {FlexTelevisionSeasonEntity} from '../season';

@ChildEntity()
export class FlexTelevisionEpisodeMetadataEntity extends FlexVideoMetadataEntity implements FlexTelevisionEpisodeMetadata {
  kind: FlexTelevisionLibraryItemKind.EPISODE = FlexTelevisionLibraryItemKind.EPISODE;

  @Column({
    default: FLEX_TELEVISION_TYPE,
  })
  type: typeof FLEX_TELEVISION_TYPE = FLEX_TELEVISION_TYPE;
  // item?: FlexVideoMediaItemEntity;

  @Column()
  episodeNumber!: number;

  @Column()
  seasonNumber!: number;
}

@ChildEntity()
export class FlexTelevisionEpisodeEntity extends FlexLibraryItemEntity implements FlexTelevisionEpisode {
  kind: FlexTelevisionLibraryItemKind.EPISODE = FlexTelevisionLibraryItemKind.EPISODE;

  @Column({
    default: FLEX_TELEVISION_TYPE,
  })
  type: typeof FLEX_TELEVISION_TYPE = FLEX_TELEVISION_TYPE;;

  declare media?: FlexVideoMediaItemEntity;
  declare metadata: FlexTelevisionEpisodeMetadataEntity;

  @ManyToOne<FlexTelevisionSeasonEntity>('FlexTelevisionSeasonEntity', season => season.episodes)
  season!: FlexTelevisionSeasonEntity;
}
