import {FlexTelevisionEpisodeEntity, FlexTelevisionEpisodeMetadataEntity} from './entity.class';
import {provideFlexDatabaseEntities} from '#/core/database';

provideFlexDatabaseEntities(
  FlexTelevisionEpisodeEntity,
  FlexTelevisionEpisodeMetadataEntity,
);
