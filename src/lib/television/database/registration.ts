import {provideLibraryScanIngress} from '#/library/database';
import {FLEX_TELEVISION_TYPE} from '../constants';
import '#/video/database/registration';
import '#/library/database/registration';
import './episode/registration';
import './library/registration';
import './season/registration';
import './show/registration';
import {flexTelevisionDatabaseScanIngress} from './scan';

provideLibraryScanIngress(FLEX_TELEVISION_TYPE, flexTelevisionDatabaseScanIngress);
