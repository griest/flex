import {FlexTelevisionLibraryEntity} from './entity.class';
import {provideFlexDatabaseEntities} from '#/core/database';
import {provideFlexLibraryDatabaseEntityType} from '#/library/database';
import {FLEX_TELEVISION_TYPE} from '#/television/constants';

provideFlexDatabaseEntities(
  FlexTelevisionLibraryEntity,
);
provideFlexLibraryDatabaseEntityType(FLEX_TELEVISION_TYPE, FlexTelevisionLibraryEntity);
