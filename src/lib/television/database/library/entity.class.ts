import {FlexLibraryEntity} from '#/library/database';
import {FLEX_TELEVISION_TYPE, FlexTelevisionLibrary} from '#/television';
import {ChildEntity, Column, OneToMany} from 'typeorm';
import type {FlexTelevisionEpisodeEntity} from '../episode';
import {FlexTelevisionSeasonEntity} from '../season';
import type {FlexTelevisionShowEntity} from '../show';

@ChildEntity()
export class FlexTelevisionLibraryEntity extends FlexLibraryEntity implements FlexTelevisionLibrary {
  @Column({
    default: FLEX_TELEVISION_TYPE,
  })
  type: typeof FLEX_TELEVISION_TYPE = FLEX_TELEVISION_TYPE;;

  declare items: (FlexTelevisionEpisodeEntity | FlexTelevisionSeasonEntity | FlexTelevisionShowEntity)[];

  @OneToMany<FlexTelevisionShowEntity>('FlexTelevisionShowEntity', show => show.library, {
    cascade: true,
    // eager: true,
  })
  shows!: FlexTelevisionShowEntity[];
}
