import type {FlexTelevisionEpisode} from './episode.interface';
import type {FlexTelevisionLibraryItemKind} from './kind.enum';
import type {FlexTelevisionLibraryItem} from './library-item.interface';
import type {FlexTelevisionMetadata} from './metadata.interface';

export interface FlexTelevisionSeasonMetadata extends FlexTelevisionMetadata {
  kind: typeof FlexTelevisionLibraryItemKind.SEASON;
  episodeCount: number;
  seasonNumber: number;
}

export interface FlexTelevisionSeason extends FlexTelevisionLibraryItem<FlexTelevisionSeasonMetadata> {
  kind: typeof FlexTelevisionLibraryItemKind.SEASON;
  episodes: FlexTelevisionEpisode[];
  media: undefined;
}
