export enum FlexTelevisionLibraryItemKind {
  SHOW = 'flexTelevisionShow',
  SEASON = 'flexTelevisionSeason',
  EPISODE = 'flexTelevisionEpisode',
}
