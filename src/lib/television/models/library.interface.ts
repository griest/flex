import type {FlexLibrary} from '#/library';
import {FLEX_TELEVISION_TYPE} from '../constants';
import type {FlexTelevisionLibraryItem} from './library-item.interface';
import type {FlexTelevisionShow} from './show.interface';

export interface FlexTelevisionLibrary extends FlexLibrary<FlexTelevisionLibraryItem> {
  type: typeof FLEX_TELEVISION_TYPE;
  shows: FlexTelevisionShow[];
}
