import type {FlexLibraryItem} from '#/library';
import type {FlexVideoMediaSource} from '#/video';
import {FLEX_TELEVISION_TYPE} from '../constants';
import type {FlexTelevisionLibraryItemKind} from './kind.enum';
import type {FlexTelevisionMetadata} from './metadata.interface';

export interface FlexTelevisionLibraryItem<
  Metadata extends FlexTelevisionMetadata = FlexTelevisionMetadata,
> extends FlexLibraryItem<Metadata, FlexVideoMediaSource> {
  type: typeof FLEX_TELEVISION_TYPE;
  kind: FlexTelevisionLibraryItemKind;
}
