import type {FlexVideoMetadata} from '#/video';
import {FLEX_TELEVISION_TYPE} from '../constants';
import type {FlexTelevisionLibraryItemKind} from './kind.enum';

export interface FlexTelevisionMetadata extends FlexVideoMetadata {
  kind: FlexTelevisionLibraryItemKind;
  type: typeof FLEX_TELEVISION_TYPE;
}
