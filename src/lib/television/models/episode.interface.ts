import type {FlexTelevisionLibraryItemKind} from './kind.enum';
import type {FlexTelevisionLibraryItem} from './library-item.interface';
import type {FlexTelevisionMetadata} from './metadata.interface';

export interface FlexTelevisionEpisodeMetadata extends FlexTelevisionMetadata {
  kind: typeof FlexTelevisionLibraryItemKind.EPISODE;
  episodeNumber: number;
  seasonNumber: number;
}

export interface FlexTelevisionEpisode extends FlexTelevisionLibraryItem<FlexTelevisionEpisodeMetadata> {
  kind: typeof FlexTelevisionLibraryItemKind.EPISODE;
}
