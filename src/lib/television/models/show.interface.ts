import type {FlexTelevisionLibraryItemKind} from './kind.enum';
import type {FlexTelevisionLibraryItem} from './library-item.interface';
import type {FlexTelevisionMetadata} from './metadata.interface';
import type {FlexTelevisionSeason} from './season.interface';

export interface FlexTelevisionShowMetadata extends FlexTelevisionMetadata {
  kind: typeof FlexTelevisionLibraryItemKind.SHOW;
  episodeCount: number;
  seasonCount: number;
}

export interface FlexTelevisionShow extends FlexTelevisionLibraryItem<FlexTelevisionShowMetadata> {
  kind: typeof FlexTelevisionLibraryItemKind.SHOW;
  seasons: FlexTelevisionSeason[];
  media: undefined;
}
