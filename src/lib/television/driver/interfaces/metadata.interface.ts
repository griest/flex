import type {FlexMetadataDriverInterface} from '#/metadata/driver';
import type {FlexTelevisionEpisodeMetadata, FlexTelevisionSeasonMetadata, FlexTelevisionShowMetadata} from '#/television';

export interface FlexTelevisionMetadataDriverResponse extends FlexTelevisionShowMetadata {
  seasons: Record<FlexTelevisionSeasonMetadata['seasonNumber'], FlexTelevisionSeasonMetadata>;
  episodes: Record<FlexTelevisionSeasonMetadata['seasonNumber'], Record<FlexTelevisionEpisodeMetadata['episodeNumber'], FlexTelevisionEpisodeMetadata>>;
}

export type FlexTelevisionMetadataDriverInterface = FlexMetadataDriverInterface<FlexTelevisionMetadataDriverResponse>;
