import type {FlexMetadataAgentSearchResult} from '#/metadata';
import {MovieDb} from 'moviedb-promise';
import type {FlexTelevisionMetadataDriverInterface, FlexTelevisionMetadataDriverResponse} from '#/television/driver';
import {tmdbTelevisionSearchResultTransform, tmdbTelevisionResponseTransform} from './transforms';
import {Service} from 'typedi';
import {compact, flatMap, keyBy} from 'lodash';

@Service()
export class TmdbTelevisionMetadataDriverService implements FlexTelevisionMetadataDriverInterface {
  constructor (
    private client: MovieDb,
  ) {}

  async search (query: string): Promise<FlexMetadataAgentSearchResult[]> {
    const resp = await this.client.searchTv({
      query,
    });

    return resp.results?.map(tmdbTelevisionSearchResultTransform) || [];
  }

  async fetch (resultId: string): Promise<FlexTelevisionMetadataDriverResponse> {
    const resp = await this.client.tvInfo(resultId);
    const seasons = keyBy(await Promise.all(
      compact(resp.seasons?.map(({season_number}) => season_number)).map(season_number =>
        this.client.seasonInfo({
          id: resultId,
          season_number,
        }),
      ),
    ), 'season_number');
    const episodes = await Promise.all(
      compact(flatMap(seasons, ({season_number, episodes}) =>
        episodes?.map(({episode_number}) =>
          season_number && episode_number
            ? this.client.episodeInfo({
              id: resultId,
              season_number,
              episode_number,
            })
            : null,
        ),
      )),
    );

    return tmdbTelevisionResponseTransform(resp, seasons, episodes, 'https://www.themoviedb.org/');
  }
}
