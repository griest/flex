import {FlexMetadataAgentSearchResult, FlexMetadataStatus} from '#/metadata';
import {FLEX_TELEVISION_TYPE} from '#/television';
import type {TvResult} from 'moviedb-promise';

export const tmdbTelevisionSearchResultTransform = (result: TvResult): FlexMetadataAgentSearchResult => ({
  status: FlexMetadataStatus.UNREFINED,
  type: FLEX_TELEVISION_TYPE,
  uid: result.id?.toString() || '',
  year: result.first_air_date ? new Date(result.first_air_date).getUTCFullYear().toString() : '',
  title: result.name || '',
  score: result.popularity || 0,
});
