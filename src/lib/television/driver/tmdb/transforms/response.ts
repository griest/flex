import {FlexTelevisionEpisodeMetadata, FlexTelevisionLibraryItemKind, FlexTelevisionSeasonMetadata, FLEX_TELEVISION_TYPE} from '#/television';
import type {Episode, ShowResponse, TvSeasonResponse} from 'moviedb-promise';
import type {FlexTelevisionMetadataDriverResponse} from '#/television/driver';
import {TMDB_ID_PREFIX} from '#/driver/tmdb';
import {groupBy, keyBy, mapValues} from 'lodash';
import {FlexMetadataStatus} from '#/metadata';

const transformSeason = (result: TvSeasonResponse, baseUrl: string, backdrop?: string | null): FlexTelevisionSeasonMetadata => ({
  uid: `${TMDB_ID_PREFIX}${result.id}`,
  status: FlexMetadataStatus.REFINED,
  type: FLEX_TELEVISION_TYPE,
  kind: FlexTelevisionLibraryItemKind.SEASON,
  image: result.poster_path && `${baseUrl}t/p/original${result.poster_path}`,
  thumbnail: result.poster_path && `${baseUrl}t/p/w220_and_h330_face${result.poster_path}`,
  backdrop,
  year: result.air_date ? new Date(result.air_date).getUTCFullYear().toString() : '',
  title: result.name || '',
  description: result.overview,
  episodeCount: result.episodes?.length || 0,
  seasonNumber: result.season_number || 0,
  cast: [],
  crew: [],
  credits: [],
});

const transformEpisode = (result: Episode, baseUrl: string, backdrop?: string | null): FlexTelevisionEpisodeMetadata => ({
  uid: `${TMDB_ID_PREFIX}${result.id}`,
  status: FlexMetadataStatus.REFINED,
  type: FLEX_TELEVISION_TYPE,
  kind: FlexTelevisionLibraryItemKind.EPISODE,
  image: result.still_path && `${baseUrl}t/p/original${result.still_path}`,
  thumbnail: result.still_path && `${baseUrl}t/p/w220_and_h330_face${result.still_path}`,
  backdrop,
  year: result.air_date ? new Date(result.air_date).getUTCFullYear().toString() : '',
  title: result.name || '',
  description: result.overview,
  episodeNumber: result.episode_number || 0,
  seasonNumber: result.season_number || 0,
  cast: [],
  crew: [],
  credits: [],
});

export const tmdbTelevisionResponseTransform = (result: ShowResponse, seasons: Record<number, TvSeasonResponse>, episodes: Episode[], baseUrl: string): FlexTelevisionMetadataDriverResponse => {
  const backdrop = result.backdrop_path && `${baseUrl}t/p/original${result.backdrop_path}`;
  return {
    uid: `${TMDB_ID_PREFIX}${result.id}`,
    status: FlexMetadataStatus.REFINED,
    type: FLEX_TELEVISION_TYPE,
    kind: FlexTelevisionLibraryItemKind.SHOW,
    image: result.poster_path && `${baseUrl}t/p/original${result.poster_path}`,
    thumbnail: result.poster_path && `${baseUrl}t/p/w220_and_h330_face${result.poster_path}`,
    backdrop,
    year: result.first_air_date ? new Date(result.first_air_date).getUTCFullYear().toString() : '',
    title: result.name || '',
    description: result.overview,
    seasonCount: result.number_of_seasons || 0,
    episodeCount: result.number_of_episodes || 0,
    seasons: mapValues(seasons, season => transformSeason(season, baseUrl, backdrop)),
    episodes: mapValues(groupBy(episodes.map(ep => transformEpisode(ep, baseUrl, backdrop)), 'seasonNumber'), eps => keyBy(eps, 'episodeNumber')),
    cast: [],
    crew: [],
    credits: [],
  };
};
