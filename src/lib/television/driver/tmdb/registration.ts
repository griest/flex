import {provideFlexMetadataDriver} from '#/metadata/driver';
import {FLEX_TELEVISION_TYPE} from '#/television';
import {TmdbTelevisionMetadataDriverService} from './metadata.class';

provideFlexMetadataDriver(FLEX_TELEVISION_TYPE, TmdbTelevisionMetadataDriverService);
