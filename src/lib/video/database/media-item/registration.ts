import {FlexVideoMediaItemEntity} from './entity.class';
import {provideFlexDatabaseEntities} from '#/core/database';

provideFlexDatabaseEntities(FlexVideoMediaItemEntity);
