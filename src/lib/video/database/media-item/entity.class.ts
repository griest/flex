import {FlexMediaItem} from '#/media';
import {FlexMediaItemEntity} from '#/media/database';
import {FlexVideoMediaSource} from '#/video';
import {ChildEntity, OneToMany} from 'typeorm';
import {FlexVideoMediaSourceEntityBase} from '../media-source';

@ChildEntity()
export class FlexVideoMediaItemEntity extends FlexMediaItemEntity implements FlexMediaItem<FlexVideoMediaSource> {
  @OneToMany<FlexVideoMediaSourceEntityBase>('FlexVideoMediaSourceEntityBase', source => source.item, {
    cascade: true,
    eager: true,
  })
  declare sources: FlexVideoMediaSourceEntityBase[];
}
