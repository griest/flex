import {FlexMetadataCreditEntity} from '#/metadata/database';
import {FlexMetadataCastCredit, FLEX_VIDEO_METADATA_CAST_KIND, FlexVideoMetadata} from '#/video';
import {ChildEntity, Column, ManyToOne} from 'typeorm';
import type {FlexVideoMetadataEntity} from '../metadata';

@ChildEntity()
export class FlexMetadataCastCreditEntity extends FlexMetadataCreditEntity implements FlexMetadataCastCredit {
  kind: typeof FLEX_VIDEO_METADATA_CAST_KIND = FLEX_VIDEO_METADATA_CAST_KIND;

  @Column()
  character!: string;

  @ManyToOne<FlexVideoMetadataEntity>('FlexVideoMetadataEntity', metadata => metadata.cast)
  declare item: FlexVideoMetadata;
}
