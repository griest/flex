import {FlexMetadataCastCreditEntity} from './entity.class';
import {provideFlexDatabaseEntities} from '#/core/database';

provideFlexDatabaseEntities(FlexMetadataCastCreditEntity);
