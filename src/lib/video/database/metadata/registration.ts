import Container from 'typedi';
import {FlexMetadataDatabaseEntityTypeRegistration, FLEX_METADATA_DATABASE_ENTITY_REGISTRATIONS} from '#/metadata/database';
import {FLEX_VIDEO_TYPE} from '#/video';
import {FlexVideoMetadataEntity} from './entity.class';
import {provideFlexDatabaseEntities} from '#/core/database';

export const flexVideoDatabaseMetadataRegistration: FlexMetadataDatabaseEntityTypeRegistration<FlexVideoMetadataEntity> = {
  type: FLEX_VIDEO_TYPE,
  entity: FlexVideoMetadataEntity,
};

Container.set<typeof flexVideoDatabaseMetadataRegistration>({
  id: FLEX_METADATA_DATABASE_ENTITY_REGISTRATIONS,
  value: flexVideoDatabaseMetadataRegistration,
  multiple: true,
});
provideFlexDatabaseEntities(FlexVideoMetadataEntity);
