import {FlexMetadataEntity} from '#/metadata/database';
import {FlexMetadataCastCredit, FlexMetadataCrewCredit, FlexVideoMetadata} from '#/video';
import {ChildEntity, OneToMany} from 'typeorm';
import type {FlexMetadataCastCreditEntity} from '../cast';
import type {FlexMetadataCrewCreditEntity} from '../crew';

@ChildEntity()
export class FlexVideoMetadataEntity extends FlexMetadataEntity implements FlexVideoMetadata {
  @OneToMany<FlexMetadataCastCreditEntity>(
    'FlexMetadataCastCreditEntity',
    cast => cast.item,
    {
      cascade: true,
    },
  )
  cast!: FlexMetadataCastCredit[];

  @OneToMany<FlexMetadataCrewCreditEntity>(
    'FlexMetadataCrewCreditEntity',
    crew => crew.item,
    {
      cascade: true,
    },
  )
  crew!: FlexMetadataCrewCredit[];
}
