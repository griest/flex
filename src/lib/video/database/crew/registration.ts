import {FlexMetadataCrewCreditEntity} from './entity.class';
import {provideFlexDatabaseEntities} from '#/core/database';

provideFlexDatabaseEntities(FlexMetadataCrewCreditEntity);
