import {FlexMetadataCreditEntity} from '#/metadata/database';
import {FlexMetadataCrewCredit, FLEX_VIDEO_METADATA_CREW_KIND, FlexVideoMetadata} from '#/video';
import {ChildEntity, Column, ManyToOne} from 'typeorm';
import type {FlexVideoMetadataEntity} from '../metadata';

@ChildEntity()
export class FlexMetadataCrewCreditEntity extends FlexMetadataCreditEntity implements FlexMetadataCrewCredit {
  kind: typeof FLEX_VIDEO_METADATA_CREW_KIND = FLEX_VIDEO_METADATA_CREW_KIND;

  @Column()
  job!: string;

  @ManyToOne<FlexVideoMetadataEntity>('FlexVideoMetadataEntity', metadata => metadata.cast)
  declare item: FlexVideoMetadata;
}
