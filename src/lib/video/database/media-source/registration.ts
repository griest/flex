import {FLEX_TYPE_DEFAULT} from '#/core';
import {FlexVideoMediaSourceEntity} from './entity.class';
import Container from 'typedi';
import {provideFlexDatabaseEntities} from '#/core/database';
import {FlexMediaDatabaseSourceEntityTypeRegistration, FLEX_MEDIA_DATABASE_SOURCE_ENTITY_REGISTRATIONS} from '#/media/database';

export const flexVideoDatabaseMediaSourceRegistration: FlexMediaDatabaseSourceEntityTypeRegistration<FlexVideoMediaSourceEntity> = {
  type: FLEX_TYPE_DEFAULT,
  entity: FlexVideoMediaSourceEntity,
};

Container.set<typeof flexVideoDatabaseMediaSourceRegistration>({
    id: FLEX_MEDIA_DATABASE_SOURCE_ENTITY_REGISTRATIONS,
    value: flexVideoDatabaseMediaSourceRegistration,
    multiple: true,
  });
  provideFlexDatabaseEntities(FlexVideoMediaSourceEntity);
