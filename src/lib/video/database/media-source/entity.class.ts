import {FlexMediaSourceEntity} from '#/media/database';
import {FlexPlayableLengthEntity} from '#/player/database';
import {FlexVideoMediaSource} from '#/video';
import {FlexViewableResolutionEntity} from '#/viewable/database';
import {ChildEntity, Column} from 'typeorm';

export abstract class FlexVideoMediaSourceEntityBase extends FlexMediaSourceEntity implements FlexVideoMediaSource {
  @Column(() => FlexViewableResolutionEntity)
  resolution!: FlexViewableResolutionEntity;

  @Column(() => FlexPlayableLengthEntity)
  length!: FlexPlayableLengthEntity;
}

@ChildEntity()
export class FlexVideoMediaSourceEntity extends FlexVideoMediaSourceEntityBase {}
