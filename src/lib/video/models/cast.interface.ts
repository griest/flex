import {FlexMetadataCredit} from '#/metadata';
import type {FlexVideoMetadata} from './metadata.interface';

export const FLEX_VIDEO_METADATA_CAST_KIND = 'flexVideoCast';

export interface FlexMetadataCastCredit extends FlexMetadataCredit {
  kind: typeof FLEX_VIDEO_METADATA_CAST_KIND;
  character: string;
  item?: FlexVideoMetadata;
}
