import {FlexMetadataCredit} from '#/metadata';
import type {FlexVideoMetadata} from './metadata.interface';

export const FLEX_VIDEO_METADATA_CREW_KIND = 'flexVideoCrew';

export interface FlexMetadataCrewCredit extends FlexMetadataCredit {
  kind: typeof FLEX_VIDEO_METADATA_CREW_KIND;
  job: string;
  item?: FlexVideoMetadata;
}
