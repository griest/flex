import type {FlexMetadata} from '#/metadata';
import type {FlexMetadataCastCredit} from './cast.interface';
import type {FlexMetadataCrewCredit} from './crew.interface';

export interface FlexVideoMetadata extends FlexMetadata {
  cast: FlexMetadataCastCredit[];
  crew: FlexMetadataCrewCredit[];
}
