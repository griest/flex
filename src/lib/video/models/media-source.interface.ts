import {FlexAudioMediaSource} from '#/audio';
import {FlexViewableMediaSource} from '#/viewable';

export interface FlexVideoMediaSource extends FlexAudioMediaSource, FlexViewableMediaSource {}
