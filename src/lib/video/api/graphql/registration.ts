import FlexVideoGraphQlSchema from './schema.gqlschema';
import {provideApiGraphQlSchema} from '#/api/graphql';
import '#/metadata/api/graphql/registration';
import '#/media/api/graphql/registration';

provideApiGraphQlSchema(FlexVideoGraphQlSchema);
