import {FlexFSEntry, FlexFSEntryType, FlexFSPath} from '#/fs';
import {Column, Entity} from 'typeorm';

export abstract class FlexFSEntryBaseEntity implements FlexFSEntry {
  @Column()
  name!: string;

  @Column('text', {
    nullable: false,
    default: FlexFSEntryType.UNKNOWN,
  })
  declare type: FlexFSEntryType;

  @Column('text', {array: true})
  path!: FlexFSPath;
}

@Entity()
export class FlexFSEntryEntity extends FlexFSEntryBaseEntity {}
