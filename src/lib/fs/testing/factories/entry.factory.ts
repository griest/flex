import {FlexFSEntry, FlexFSEntryType} from '#/fs';

export class MockFlexFSEntry implements FlexFSEntry {
  type = FlexFSEntryType.FILE;
  uid = 'uid';
  name = 'name';
  path = ['random', 'path'];
}
