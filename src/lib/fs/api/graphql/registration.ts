import FlexFSGraphQlSchema from './schema.gqlschema';
import {provideApiGraphQlSchema} from '#/api/graphql';

provideApiGraphQlSchema(FlexFSGraphQlSchema);
