import {FlexFSEntryType} from './entry-type.enum';
import {FlexFSEntry} from './entry.interface';

export interface FlexFSDirectory extends FlexFSEntry {
  type: FlexFSEntryType.DIRECTORY;
}
