export enum FlexFSEntryType {
  FILE = 'FILE',
  DIRECTORY = 'DIRECTORY',
  UNKNOWN = 'UNKNOWN',
}
