// TODO: pare down this type to a subset of chars
export type FlexFSPathSegment = string;
export type FlexFSPath = FlexFSPathSegment[];
