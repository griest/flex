import {FlexFSEntryType} from './entry-type.enum';
import {FlexFSEntry} from './entry.interface';

export interface FlexFSFile extends FlexFSEntry {
  type: FlexFSEntryType.FILE;
  /**
   * The file size, in bytes.
   */
  size: number;
  extension: string;
}
