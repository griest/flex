import {FlexFSEntry} from './entry.interface';

export interface FlexFSEntryInput {
  path: FlexFSEntry['path'];
}
