import {FlexTypeable} from '#/core';
import {FlexFSEntryType} from './entry-type.enum';
import {FlexFSPath} from './path.type';

export interface FlexFSEntry extends FlexTypeable {
  type: FlexFSEntryType;
  name: string;
  /**
   * The path of the entry, represented as a list of path segments.
   */
  path: FlexFSPath;
}
