import {FlexFSEntryType, FlexFSFile, FlexFSPath} from '#/fs';
import Container, {Service} from 'typedi';
import {FlexLocalReadonlyFSDriver} from '#/fs/driver/local';

export interface FlexFSScanOptions {
  /**
   * The maximum subdirectory depth that will be traversed while scanning.
   */
  maxDepth: number;
  /**
   * The file extensions for which to scan.
   * Files without one of these extensions, or no extension, will not be included in the results.
   *
   * Set to an empty array to include all file extensions.
   */
  extensions: string[];
  /**
   * The max file size in bytes.
   * Files larger than this will not be included in the results.
   *
   * Set to null to disable this check.
   */
  maxSize: number | null;
  /**
   * The min file size in bytes.
   * Files smaller than this will not be included in the results.
   *
   * Set to null to disable this check.
   */
  minSize: number | null;
}

const defaultScanOptions: FlexFSScanOptions = {
  maxDepth: 5,
  extensions: [],
  maxSize: null,
  minSize: null,
};

@Service()
export class FlexFSScanService {
  constructor (
    private fsDriver: FlexLocalReadonlyFSDriver,
  ) {}

  async scan (directories: FlexFSPath[], options: Partial<FlexFSScanOptions> = defaultScanOptions): Promise<FlexFSFile[]> {
    const opts: FlexFSScanOptions = {
      ...defaultScanOptions,
      ...options,
    };
    return Promise.all(directories.map(directory => this.scanDirectory(directory, opts))).then(files => files.flat());
  }

  private async scanDirectory (directory: FlexFSPath, options: FlexFSScanOptions = defaultScanOptions): Promise<FlexFSFile[]> {
    const traverseTree = async (dir: FlexFSPath, depthRemaining: number): Promise<FlexFSFile[]> => {
      const driver = Container.get(FlexLocalReadonlyFSDriver);
      const entries = await driver.list(dir);
      const files = <FlexFSFile[]>entries.filter(entry => entry.type === FlexFSEntryType.FILE && this.testFile(<FlexFSFile>entry, options));

      if (depthRemaining <= 1) {
        return files;
      } else {
        const children = entries.filter(entry =>
          entry.type === FlexFSEntryType.DIRECTORY,
        ).map(d =>
          traverseTree(d.path, depthRemaining - 1),
        );

        return [
          ...files,
          ...await Promise.all(children).then(files => files.flat()),
        ];
      }
    };

    return traverseTree(directory, options.maxDepth);
  }

  private testFile (file: FlexFSFile, options: FlexFSScanOptions = defaultScanOptions): boolean {
    if (options.extensions?.length > 0 && !options.extensions.includes(file.extension)) {
      return false;
    }
    if (options.maxSize && file.size > options.maxSize) {
      return false;
    }
    if (options.minSize && file.size < options.minSize) {
      return false;
    }

    return true;
  }
}
