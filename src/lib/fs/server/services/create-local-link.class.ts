import {Inject, Service} from 'typedi';
import express, {Express} from 'express';
import {FLEX_SERVER_APP_TOKEN} from '#/server';
import {FlexFSPath} from '#/fs';

@Service()
export class FlexServerCreateLocalLink {
  constructor (
    @Inject(FLEX_SERVER_APP_TOKEN) private app: Express,
  ) {}

  createLink (localPath: FlexFSPath): string {
    const fullPath = localPath.join('/');
    const dirPath = localPath.slice(0, localPath.length - 1).join('/');
    const fileName = localPath.slice(localPath.length - 1).join('/');

    this.app.use(fullPath, express.static(dirPath, {index: fileName}));

    return `${global.location.hostname}${fullPath}`;
  }
}
