import {Service} from 'typedi';
import {FlexFSDirectory, FlexFSEntry, FlexFSEntryType, FlexFSFile, FlexFSPath} from '#/fs';
import {Dirent, Stats} from 'fs';
import {opendir, stat} from 'fs/promises';
import {extname, join, parse, resolve} from 'path';
import {FlexReadonlyFSDriverInterface} from '../interfaces';

const flexServerFSTransformDirentToEntry = (dirent: Dirent, parentPath: FlexFSPath): FlexFSEntry => ({
  type: FlexFSEntryType.UNKNOWN,
  name: parse(dirent.name).name,
  path: [
    ...parentPath,
    dirent.name,
  ],
});
const flexServerFSTransformDirentToFile = (dirent: Dirent, parentPath: FlexFSPath, stats: Stats): FlexFSFile => ({
  ...flexServerFSTransformDirentToEntry(dirent, parentPath),
  type: FlexFSEntryType.FILE,
  size: stats.size,
  extension: extname(dirent.name),
});
const flexServerFSTransformDirentToDirectory = (dirent: Dirent, parentPath: FlexFSPath): FlexFSDirectory => ({
  ...flexServerFSTransformDirentToEntry(dirent, parentPath),
  type: FlexFSEntryType.DIRECTORY,
});

@Service()
export class FlexLocalReadonlyFSDriver implements FlexReadonlyFSDriverInterface {
  async list (dirPath: FlexFSPath) {
    const path = resolve('/', ...dirPath);
    const dir = await opendir(path);
    const entries: FlexFSEntry[] = [];

    for await (const dirent of dir) {
      if (dirent?.isDirectory()) {
        entries.push(flexServerFSTransformDirentToDirectory(dirent, dirPath));
      } else if (dirent?.isFile()) {
        entries.push(flexServerFSTransformDirentToFile(dirent, dirPath, await stat(join(path, dirent.name))));
      }
    }

    return entries;
  }
}
