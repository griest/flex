import {Constructable, Token} from 'typedi';
import {FlexFSPath} from '#/fs';

// TODO: move to media
export interface FlexFSLinkDriverInterface {
  /**
   * Gets a link with which the file can be read.
   */
  getLink(protocol: string, filePath: FlexFSPath): Promise<string>;
}

export const FLEX_FS_LINK_DRIVER = new Token<Constructable<FlexFSLinkDriverInterface>>('FLEX_FS_LINK_DRIVER');
