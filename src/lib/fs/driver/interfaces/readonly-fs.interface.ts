import {Constructable, Token} from 'typedi';
import {FlexFSEntry, FlexFSPath} from '#/fs';

export enum FlexReadonlyFSDriverType {
  LOCAL = 'LOCAL',
  HTTP = 'HTTP',
}

export interface FlexReadonlyFSDriverInterface {
  /**
   * List the contents of a directory.
   */
  list(dirPath: FlexFSPath): Promise<FlexFSEntry[]>;
}

export const FLEX_READONLY_FS_DRIVER = new Token<Constructable<FlexReadonlyFSDriverInterface>>('FLEX_READONLY_FS_DRIVER');
