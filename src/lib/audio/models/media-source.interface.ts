import {FlexPlayableMediaSource} from '#/player';

export type FlexAudioMediaSource = FlexPlayableMediaSource;
