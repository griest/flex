import {FlexLibrary} from '#/library';
import {FlexMusicLibraryItem} from './library-item.interface';

export type FlexMusicLibrary = FlexLibrary<FlexMusicLibraryItem>;
