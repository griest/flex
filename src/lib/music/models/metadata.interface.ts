import {FlexMetadata} from '#/metadata';

export interface FlexMusicMetadata extends FlexMetadata {
  artist: string;
  studio: string;
}
