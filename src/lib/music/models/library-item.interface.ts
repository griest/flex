import {FlexLibraryItem} from '#/library';
import {FlexVideoMediaSource} from '#/video';
import {FlexMusicMetadata} from './metadata.interface';

export type FlexMusicLibraryItem = FlexLibraryItem<FlexMusicMetadata, FlexVideoMediaSource>;
