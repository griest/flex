import {Container} from 'typedi';
import {FLEX_PLAYER_DRIVER} from '../interfaces';
import {FlexPlayerTestingDriver} from './player.class';

export const flexTestingDriverRegistration = (): void => {
  // console.log('driver');

  Container.set(FLEX_PLAYER_DRIVER, FlexPlayerTestingDriver);
};
