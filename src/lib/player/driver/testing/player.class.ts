import {Service} from 'typedi';
import {FlexPlayerDriverInterface} from '#/player/driver';
import {FlexPlayableControl, FlexPlayableMediaSource} from '#/player';
import {MockFlexPlayableControl} from '#/player/testing';

@Service()
export class FlexPlayerTestingDriver implements FlexPlayerDriverInterface {
  async play (source: FlexPlayableMediaSource): Promise<FlexPlayableControl> {
    return new MockFlexPlayableControl();
  }
}
