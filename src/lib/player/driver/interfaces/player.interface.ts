import {FlexPlayableControl, FlexPlayableMediaSource} from '#/player';
import {Constructable, Token} from 'typedi';

export interface FlexPlayerDriverInterface {
  play(source: FlexPlayableMediaSource): Promise<FlexPlayableControl>;
}

export const FLEX_PLAYER_DRIVER = new Token<Constructable<FlexPlayerDriverInterface>>('FLEX_PLAYER_DRIVER');
