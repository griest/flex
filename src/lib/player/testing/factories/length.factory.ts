import {FlexPlayableLength, FlexPlayableLengthUnit} from '#/player';

export class MockFlexPlayableLength implements FlexPlayableLength {
  length = 0;
  units = FlexPlayableLengthUnit.SECONDS;
}
