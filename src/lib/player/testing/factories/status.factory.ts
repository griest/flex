import {FlexPlayableStatus, FlexPlaybackStatus} from '#/player';
import {MockFlexPlayableLength} from './length.factory';

export class MockFlexPlayableStatus implements FlexPlayableStatus {
  status = FlexPlaybackStatus.PLAYING;
  position = new MockFlexPlayableLength();
}
