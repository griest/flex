import {FlexPlayableControl} from '#/player';
import {BehaviorSubject} from 'rxjs';
import {MockFlexPlayableStatus} from './status.factory';

export class MockFlexPlayableControl implements FlexPlayableControl {
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  play () {}
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  pause () {}

  status$ = new BehaviorSubject(new MockFlexPlayableStatus());
}
