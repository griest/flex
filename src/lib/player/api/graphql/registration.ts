import FlexPlayerGraphQlSchema from './schema.gqlschema';
import {provideApiGraphQlSchema} from '#/api/graphql';

provideApiGraphQlSchema(FlexPlayerGraphQlSchema);
