import {FlexMediaSource} from '#/media';
import {FlexPlayableLength} from './length.interface';

export interface FlexPlayableMediaSource extends FlexMediaSource {
  length: FlexPlayableLength;
}
