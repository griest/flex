export enum FlexPlayableLengthUnit {
  HOURS = 'hours',
  MINUTES = 'minutes',
  SECONDS = 'seconds',
  MILLISECONDS = 'milliseconds',
}

export interface FlexPlayableLength {
  length: number;
  units: FlexPlayableLengthUnit;
}
