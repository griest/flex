import {FlexPlayableStatus} from './status.interface';
import {Observable} from 'rxjs';

/**
 * A way to control a playing media source.
 */
export interface FlexPlayableControl {
  /**
   * Play the media source.
   */
  play(): void;
  /**
   * Pause the media source.
   */
  pause(): void;

  status$: Observable<FlexPlayableStatus>;
}
