import {FlexPlayableLength} from './length.interface';

/**
 * Represents in what state the playable source is.
 */
export enum FlexPlaybackStatus {
  PLAYING = 'playing',
  PAUSED = 'paused',
  BUFFERING = 'buffering',
}

/**
 * A collection of information about the playback state of a playable media source.
 */
export interface FlexPlayableStatus {
  /**
   * Where in the playable media source the current playback is.
   */
  position: FlexPlayableLength;
  /**
   * The playback status of the playable source.
   */
  status: FlexPlaybackStatus;
}
