import {FlexPlayableLength, FlexPlayableLengthUnit} from '#/player';
import {Column} from 'typeorm';

export class FlexPlayableLengthEntity implements FlexPlayableLength {
  @Column('text', {
    nullable: false,
    default: FlexPlayableLengthUnit.SECONDS,
  })
  units!: FlexPlayableLengthUnit;

  @Column()
  length!: number;
}
