import {FlexPlayableLengthEntity} from './entity.class';
import {provideFlexDatabaseEntities} from '#/core/database';

provideFlexDatabaseEntities(FlexPlayableLengthEntity);
