import {createMultiToken} from '#/core/di';

export const {
  token: FLEX_API_GRAPHQL_SCHEMA,
  provide: provideApiGraphQlSchema,
  inject: injectApiGraphQlSchema,
} = createMultiToken<string>('FLEX_API_GRAPHQL_SCHEMA');
