import {provideApiGraphQlSchema} from './schema.token';
import FlexApiGraphQlSchema from './schema.gqlschema';

provideApiGraphQlSchema(FlexApiGraphQlSchema);
