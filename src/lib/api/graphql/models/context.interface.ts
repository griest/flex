import {DataSource} from 'typeorm';

export interface FlexApiGraphQlContext {
  db: DataSource;
}
