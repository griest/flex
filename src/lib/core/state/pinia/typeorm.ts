import {FlexIdentifiable} from '#/core';
import {FlexEntity, injectFlexDatabaseSource} from '#/core/database';
import {DefineStoreOptions, StateTree, Store} from 'pinia';
import {EntityTarget, FindOptionsWhereProperty, In, Repository} from 'typeorm';
// TODO: export
import {QueryDeepPartialEntity} from 'typeorm/query-builder/QueryPartialEntity';

export type TypeORMStore<T extends FlexIdentifiable = FlexIdentifiable> = Store<string, TypeORMEntityState<T>, TypeORMEntityGetters<T>, TypeORMEntityActions<T>>;
// eslint-disable-next-line
export interface TypeORMEntityState<T extends FlexIdentifiable = FlexIdentifiable> extends StateTree {}
export interface TypeORMEntityGetters<T extends FlexIdentifiable = FlexIdentifiable> {
  entity: () => EntityTarget<FlexEntity<T>>;
  repo: () => Repository<FlexEntity<T>>;
}
export interface TypeORMEntityActions<T extends FlexIdentifiable = FlexIdentifiable> {
  list: (this: TypeORMStore<T>, ...relations: string[]) => Promise<FlexEntity<T>[]>;
  get: (this: TypeORMStore<T>, id: T['uid'], ...relations: string[]) => Promise<FlexEntity<T>>;
  add: (this: TypeORMStore<T>, ...entities: T[]) => Promise<FlexEntity<T>[]>;
  set: (this: TypeORMStore<T>, ...entities: T[]) => Promise<FlexEntity<T>[]>;
  remove: (this: TypeORMStore<T>, ...ids: T['uid'][]) => Promise<Optional<number>>;
  upsert: (this: TypeORMStore<T>, ...entities: QueryDeepPartialEntity<T>[]) => Promise<FlexEntity<T>[]>;
  clear: (this: TypeORMStore<T>) => Promise<void>;
}

export const getTypeORMStoreOptions = <T extends FlexIdentifiable = FlexIdentifiable>(entity: EntityTarget<FlexEntity<T>>): Omit<DefineStoreOptions<string, TypeORMEntityState<T>, TypeORMEntityGetters<T>, TypeORMEntityActions<T>>, 'id'> => ({
  state: () => ({}),
  getters: {
    entity () {
      return entity;
    },
    repo () {
      return injectFlexDatabaseSource().getRepository(entity);
    },
  },
  // use transactions for these?
  actions: {
    async list (...relations: string[]) {
      return this.repo.find({
        relations,
      });
    },
    async get (id: T['uid'], ...relations: string[]) {
      return this.repo.findOneOrFail({
        where: {
          uid: In([id]) as FindOptionsWhereProperty<NonNullable<T['uid']>>,
        },
        relations,
      });
    },
    async add (...entities: T[]) {
      return this.repo.save(entities.map(e => this.repo.create(e)));
    },
    async set (...entities: T[]) {
      // await this.repo.clear();
      return this.repo.save(entities.map(e => this.repo.create(e)));
    },
    async remove (...ids: T['uid'][]) {
      return (await this.repo.delete(ids)).affected;
    },
    async upsert (...entities: QueryDeepPartialEntity<T>[]) {
      return (await this.repo.upsert(entities, ['uid'])).generatedMaps as FlexEntity<T>[];
    },
    async clear () {
      return this.repo.clear();
    },
  },
});
