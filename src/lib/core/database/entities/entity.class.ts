import {FlexIdentifiable} from '#/core';
import {PrimaryGeneratedColumn} from 'typeorm';

export abstract class FlexEntity<T extends FlexIdentifiable = FlexIdentifiable> implements FlexIdentifiable {
  @PrimaryGeneratedColumn('uuid')
  uid!: T['uid'];
}
