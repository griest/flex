import {createMultiToken} from '#/core/di';

export const {
  token: FLEX_DATABASE_ENTITY_REGISTRATIONS,
  provide: provideFlexDatabaseEntities,
  inject: injectFlexDatabaseEntities,
} = createMultiToken<any>('FLEX_DATABASE_ENTITY_REGISTRATIONS');
