import type {FlexTypeable} from '#/core';
import {FlexIdentifiable} from '#/core';
import {Column, Index} from 'typeorm';
import {FlexEntity} from './entity.class';

interface TypeableIdentifiableEntity extends FlexIdentifiable, FlexTypeable {}

export abstract class FlexTypeableEntity<T extends TypeableIdentifiableEntity = TypeableIdentifiableEntity> extends FlexEntity<T> implements TypeableIdentifiableEntity {
  @Column('text')
  @Index()
  type!: T['type'];
}
