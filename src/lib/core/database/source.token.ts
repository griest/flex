import {DataSource} from 'typeorm';
import {createSingleToken} from '#/core/di';

export const {
  token: FLEX_DATABASE_SOURCE,
  provide: provideFlexDatabaseSource,
  inject: injectFlexDatabaseSource,
} = createSingleToken<DataSource>('FLEX_DATABASE_SOURCE');
