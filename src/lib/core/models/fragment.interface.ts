import {FlexIdentifiable} from './identifiable.interface';

export type FlexFragment<T extends FlexIdentifiable = FlexIdentifiable> = T | (FlexIdentifiable & Partial<T>);
