import {FLEX_TYPE_DEFAULT} from '../constants';

export interface FlexTypeable {
  /**
   * The library type.
   */
  type: string | typeof FLEX_TYPE_DEFAULT;
}
