import {uniq} from 'lodash';
import Container, {Token} from 'typedi';

export interface MultiToken<T> {
  token: Token<T>;
  provide: (...values: T[]) => void;
  inject: () => T[];
}

export const createMultiToken = <T>(name: string): MultiToken<T> => {
  const token = new Token<T>(name);
  return {
    token,
    provide: (...values: T[]) => {
      values.forEach(value => {
        Container.set({
          id: token,
          value,
          multiple: true,
        });
      });
    },
    inject: () => uniq(Container.getMany(token)),
  };
};
