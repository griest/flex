import Container, {Token} from 'typedi';

export interface SingleToken<T> {
  token: Token<T>;
  provide: (value: T) => void;
  inject: () => T;
}

export const createSingleToken = <T>(name: string): SingleToken<T> => {
  const token = new Token<T>(name);
  return {
    token,
    provide: (value: T) => {
      Container.set({
        id: token,
        value,
      });
    },
    inject: () => Container.get(token),
  };
};
