import Container, {Token} from 'typedi';

export interface ScopedSingleToken<T> {
  token: Token<T>;
  provide: (scope: string, value: T) => void;
  inject: (scope: string) => T;
}

export const createScopedSingleToken = <T>(name: string): ScopedSingleToken<T> => {
  const token = new Token<T>(name);
  return {
    token,
    provide: (scope, value) => {
      Container.of(scope).set({
        id: token,
        value,
      });
    },
    inject: scope => Container.of(scope).get(token),
  };
};
