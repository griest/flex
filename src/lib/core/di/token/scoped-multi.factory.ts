import Container, {Token} from 'typedi';

export interface ScopedMultiToken<T> {
  token: Token<T>;
  provide: (scope: string, ...values: T[]) => void;
  inject: (scope: string) => T[];
}

export const createScopedMultiToken = <T>(name: string): ScopedMultiToken<T> => {
  const token = new Token<T>(name);
  return {
    token,
    provide: (scope, ...values: T[]) => {
      values.forEach(value => {
        Container.of(scope).set({
          id: token,
          value,
          multiple: true,
        });
      });
    },
    inject: scope => Container.of(scope).getMany(token),
  };
};
