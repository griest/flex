import FlexMediaGraphQlSchema from './schema.gqlschema';
import {provideApiGraphQlSchema} from '#/api/graphql';
import '#/fs/api/graphql/registration';

provideApiGraphQlSchema(FlexMediaGraphQlSchema);
