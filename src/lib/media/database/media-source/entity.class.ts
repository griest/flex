import {FlexTypeableEntity} from '#/core/database';
import {FlexFSFile} from '#/fs';
import {FlexMediaSource} from '#/media';
import {Column, Entity, ManyToOne, TableInheritance} from 'typeorm';
import {FlexMediaItemEntityBase} from '../media-item';

export abstract class FlexMediaSourceEntityBase extends FlexTypeableEntity<FlexMediaSource> implements FlexMediaSource {
  @Column('simple-json')
  file!: FlexFSFile;

  @ManyToOne<FlexMediaItemEntityBase>('FlexMediaItemEntityBase', item => item.sources)
  item!: FlexMediaItemEntityBase;
}

@Entity()
@TableInheritance({column: {name: 'entityType', type: 'varchar'}})
export class FlexMediaSourceEntity extends FlexMediaSourceEntityBase {}
