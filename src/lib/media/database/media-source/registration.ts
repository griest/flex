import {FLEX_TYPE_DEFAULT} from '#/core';
import {FlexMediaDatabaseSourceEntityTypeRegistration, FLEX_MEDIA_DATABASE_SOURCE_ENTITY_REGISTRATIONS} from './token';
import {FlexMediaSourceEntity} from './entity.class';
import Container from 'typedi';
import {provideFlexDatabaseEntities} from '#/core/database';

export const flexMediaDatabaseSourceRegistration: FlexMediaDatabaseSourceEntityTypeRegistration<FlexMediaSourceEntity> = {
  type: FLEX_TYPE_DEFAULT,
  entity: FlexMediaSourceEntity,
};

Container.set<typeof flexMediaDatabaseSourceRegistration>({
  id: FLEX_MEDIA_DATABASE_SOURCE_ENTITY_REGISTRATIONS,
  value: flexMediaDatabaseSourceRegistration,
  multiple: true,
});
provideFlexDatabaseEntities(FlexMediaSourceEntity);
