import {Constructable, Token} from 'typedi';
import type {FlexMediaSourceEntityBase} from './entity.class';

export interface FlexMediaDatabaseSourceEntityTypeRegistration<
  Entity extends FlexMediaSourceEntityBase = FlexMediaSourceEntityBase,
> {
  type: string;
  entity?: Constructable<Entity>;
}

export const FLEX_MEDIA_DATABASE_SOURCE_ENTITY_REGISTRATIONS = new Token<FlexMediaDatabaseSourceEntityTypeRegistration>('FLEX_MEDIA_DATABASE_SOURCE_ENTITY_REGISTRATIONS');
