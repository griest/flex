import {FlexEntity} from '#/core/database';
import {FlexMediaItem} from '#/media';
import {Entity, OneToMany, TableInheritance} from 'typeorm';
import type {FlexMediaSourceEntityBase} from '../media-source';

export abstract class FlexMediaItemEntityBase extends FlexEntity<FlexMediaItem> implements FlexMediaItem {
  @OneToMany<FlexMediaSourceEntityBase>('FlexMediaSourceEntityBase', source => source.item, {
    cascade: true,
    eager: true,
  })
  sources!: FlexMediaSourceEntityBase[];
}

@Entity()
@TableInheritance({column: {name: 'entityType', type: 'varchar'}})
export class FlexMediaItemEntity extends FlexMediaItemEntityBase {}
