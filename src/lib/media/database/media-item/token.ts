import {Constructable, Token} from 'typedi';
import type {FlexMediaItemEntityBase} from './entity.class';

export interface FlexMediaDatabaseItemEntityTypeRegistration<
  Entity extends FlexMediaItemEntityBase = FlexMediaItemEntityBase,
> {
  type: string;
  entity?: Constructable<Entity>;
}

export const FLEX_MEDIA_DATABASE_ITEM_ENTITY_REGISTRATIONS = new Token<FlexMediaDatabaseItemEntityTypeRegistration>('FLEX_MEDIA_DATABASE_ITEM_ENTITY_REGISTRATIONS');
