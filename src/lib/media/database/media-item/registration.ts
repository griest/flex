import {FLEX_TYPE_DEFAULT} from '#/core';
import {FlexMediaDatabaseItemEntityTypeRegistration, FLEX_MEDIA_DATABASE_ITEM_ENTITY_REGISTRATIONS} from './token';
import {FlexMediaItemEntity} from './entity.class';
import Container from 'typedi';
import {provideFlexDatabaseEntities} from '#/core/database';

export const flexMediaDatabaseItemRegistration: FlexMediaDatabaseItemEntityTypeRegistration<FlexMediaItemEntity> = {
  type: FLEX_TYPE_DEFAULT,
  entity: FlexMediaItemEntity,
};

Container.set<typeof flexMediaDatabaseItemRegistration>({
  id: FLEX_MEDIA_DATABASE_ITEM_ENTITY_REGISTRATIONS,
  value: flexMediaDatabaseItemRegistration,
  multiple: true,
});
provideFlexDatabaseEntities(FlexMediaItemEntity);
