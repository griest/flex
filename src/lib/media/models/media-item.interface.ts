import {FlexIdentifiable} from '#/core';
import {FlexMediaSource} from './media-source.interface';

export interface FlexMediaItem<
  MediaSource extends FlexMediaSource = FlexMediaSource,
> extends FlexIdentifiable {
  /**
   * A list of data sources for this media item.
   */
  sources: MediaSource[];
}
