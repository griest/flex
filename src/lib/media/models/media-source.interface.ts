import {FlexIdentifiable, FlexTypeable} from '#/core';
import {FlexFSFile} from '#/fs';

export interface FlexMediaSource extends FlexIdentifiable, FlexTypeable {
  /**
   * The location of the media source on the server, relative to the associated library root.
   */
  file: FlexFSFile;
}
