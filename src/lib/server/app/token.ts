import {Token} from 'typedi';
import type {Express} from 'express';

export const FLEX_SERVER_APP_TOKEN = new Token<Express>('FLEX_SERVER_APP');
