export const awaitValues = async <
  TValue = unknown,
>(collection: Record<string, Promise<TValue>>): Promise<Record<string, TValue>> => {
  const entries = Object.entries<Promise<TValue>>(collection);
  const resolved = await Promise.all(entries.map(o => o[1]));

  return resolved.reduce<Record<string, TValue>>((acc, val, index) => {
    acc[entries[index][0]] = val;
    return acc;
  }, {});
};
