export const createInverseRefMap = (list: any, prop: any) =>
  list.reduce((acc: any, item: any) => {
    const child = item[prop];
    if (child) {
      acc[child.uid] = item;
    }
    return acc;
  }, {});
