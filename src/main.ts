import {createApp} from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import vuetify from './plugins/vuetify';
import {loadFonts} from './plugins/webfontloader';
import loadSql from './plugins/sql.js';
import {createPinia} from 'pinia';
import '#/television/registration';
import '#/movie/registration';
import '#/movie/database/registration';
import '#/television/database/registration';
import '#/driver/tmdb/registration';
import '#/movie/driver/tmdb/registration';
import '#/television/driver/tmdb/registration';
import '#/driver/rest/registration';
import '#/library/driver/rest/registration';
import {provideDriverRestClientConfig} from '#/driver/rest';
import {injectFlexDatabaseEntities, provideFlexDatabaseSource} from '#/core/database';
import {DataSource} from 'typeorm';

const getDataSource = async () => {
  const entities = injectFlexDatabaseEntities();
  const dataSource = new DataSource({
    ...await loadSql(),
    type: 'sqljs',
    entities,
    logging: ['error'],
    synchronize: true,
    useLocalForage: true,
  });
  await dataSource.initialize();
  provideFlexDatabaseSource(dataSource);
  return dataSource;
};

const config = () => {
  provideDriverRestClientConfig({
    baseUrl: 'http://localhost:3000',
  });
  // provideDriverTmdbClientConfig({
  //   apiKey: process.env.TMDB_API_KEY || '',
  //   baseUrl: process.env.TMDB_BASE_URL || '',
  // });
};

config();

loadFonts();

getDataSource().then(() => {
  createApp(App)
    .use(router)
    .use(store)
    .use(vuetify)
    .use(createPinia())
    .mount('#app');
});
