import '~/plugins/typedi';
import express from 'express';
import cors from 'cors';
import * as fs from 'fs';
import {graphqls2s} from 'graphql-s2s';
import gql from 'graphql-tag';
import {Column, DataSource, Entity, PrimaryGeneratedColumn, Repository} from 'typeorm';
import {flexLibraryAPIGetLibraries, flexLibraryAPIAddLibrary} from '#/library/api';
import Container, {Constructable} from 'typedi';
import {graphqlHTTP} from 'express-graphql';
import {makeExecutableSchema} from '@graphql-tools/schema';
import {mergeTypeDefs, mergeResolvers} from '@graphql-tools/merge';
import {flexLibraryGraphQlResolvers, FlexLibraryGraphQlSchema} from '#/library/api/graphql';
import {print} from 'graphql';
import {FlexTelevisionGraphQlSchema} from '#/television/api/graphql';
import {FlexFSGraphQlSchema} from '#/fs/api/graphql';
import {FlexMediaGraphQlSchema} from '#/media/api/graphql';
import {FlexMetadataGraphQlSchema} from '#/metadata/api/graphql';
import {flexServerAPIGraphQlRootValue} from '#/server/api/graphql';
import '#/metadata/database';
import {FlexLibraryEntity} from '#/library/database';
import {FLEX_DATABASE_ENTITY_REGISTRATIONS, FLEX_DATABASE_SOURCE, injectFlexDatabaseEntities, injectFlexDatabaseSource, provideFlexDatabaseSource} from '#/core/database';
import '#/television/registration';
import '#/movie/registration';
import '#/movie/database/registration';
import '#/television/database/registration';
import '#/movie/api/graphql/registration';
import '#/television/api/graphql/registration';
import '#/driver/tmdb/registration';
import '#/movie/driver/tmdb/registration';
import '#/television/driver/tmdb/registration';
import '#/driver/rest/registration';
import '#/library/driver/rest/registration';
import {provideDriverTmdbClientConfig} from '#/driver/tmdb';
import {flexLibraryAPIGetLink} from '#/library/api/get-link';
import {FlexLibrary} from '#/library';
import type {RequestHandler} from 'express';
import {flexLibraryAPIScan} from '#/library/api/scan-library';
import {FlexVideoGraphQlSchema} from '#/video/api/graphql';
import {FlexApiGraphQlContext, FlexCoreGraphQlSchema, injectApiGraphQlSchema} from '#/api/graphql';
// import '#/video/database';

// useContainer(Container)

/// /// monkey patch
function isClassNameInProtoChain (name: string, target: Constructable<any>): boolean {
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  const functionProto = Object.getPrototypeOf(() => {});

  function getParentNames (target: Constructable<any>): string[] {
    const parent = Object.getPrototypeOf(target);
    return parent && parent !== functionProto
      ? [
        parent.name,
        ...getParentNames(parent),
      ]
      : [];
  }
  const names = getParentNames(target);

  return names.includes(name);
}

(global as any).isClassNameInProtoChain = isClassNameInProtoChain;
/// ////
const combinedSchema = injectApiGraphQlSchema().reduce((acc, s) => acc.concat(s), '');
const schemaDef = graphqls2s.transpileSchema(combinedSchema);
const typeDefs = mergeTypeDefs(gql(schemaDef));
const resolvers = mergeResolvers([
  flexLibraryGraphQlResolvers,
]);

const app = express();
const PORT = 3000;
const initProms = [];
const schema = makeExecutableSchema({
  typeDefs,
  resolvers,
});

fs.writeFileSync('.output/debug.gql', schemaDef);

const config = () => {
  provideDriverTmdbClientConfig({
    apiKey: process.env.TMDB_API_KEY || '',
    baseUrl: process.env.TMDB_BASE_URL || '',
  });
};

config();

async function clearDB () {
  const dataSource = injectFlexDatabaseSource();
  const entities = dataSource.entityMetadatas;
  for (const entity of entities) {
    const repository = await dataSource.getRepository(entity.name);
    await repository.query(`TRUNCATE TABLE ${entity.tableName} CASCADE;`);
  }
}

const clear: RequestHandler<Record<string, unknown>, FlexLibrary[]> = async (_req, res) => {
  await clearDB();
  const dataSource = injectFlexDatabaseSource();
  const repo = dataSource.getRepository(FlexLibraryEntity);
  const libraries = await repo.find();
  res.send(libraries);
};

const startDB = async () => {
  const entities = injectFlexDatabaseEntities();

  const dataSource = new DataSource({
    type: 'postgres',
    host: 'database',
    port: 5432,
    username: 'postgres',
    password: 'mysecretpassword',
    database: 'test',
    entities,
    logging: ['error'],
    synchronize: true,
  });
  await dataSource.initialize();
  provideFlexDatabaseSource(dataSource);
  // await clearDB();
  return dataSource;
};

// @Entity()
// class TestArrayEntity {
//   @PrimaryGeneratedColumn()
//   id!: string;

//   @Column('simple-json')
//   test!: string[][];
// }

initProms.push(
  startDB(),
);

app.use(express.json());
app.use('/source/movies', express.static('/mnt/media/Movies/007'));
app.use(cors());
app.use(
  '/graphql',
  graphqlHTTP(async () => ({
    schema,
    graphiql: process.env.NODE_ENV === 'development',
    rootValue: flexServerAPIGraphQlRootValue,
    context: <FlexApiGraphQlContext>{
      db: injectFlexDatabaseSource(),
    },
  })),
);
app.get('/', async (req, res) => {
  res.send('Hello World');
});
app.get('/libraries', flexLibraryAPIGetLibraries);
app.get('/play/link', flexLibraryAPIGetLink);
app.post('/libraries', flexLibraryAPIAddLibrary);
app.post('/clear', clear);
app.post('/scan', flexLibraryAPIScan);

Promise.all(initProms).then(() => {
  // repo = connection.getRepository(TestArrayEntity);
  // const ent = repo.create({
  //   test: [['test']],
  // });
  // repo.save(ent);
  app.listen(PORT, () => {
    console.log(`Server is listening on port ${PORT}`);
  });
});
