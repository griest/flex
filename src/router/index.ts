import {createRouter, createWebHashHistory, RouteRecordRaw} from 'vue-router';
import HomeView from '../views/HomeView.vue';

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'home',
    component: HomeView,
  },
  {
    path: '/about',
    name: 'about',
    component: () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue'),
  },
  {
    path: '/libraries',
    name: 'libraries',
    component: () => import(/* webpackChunkName: "libraries" */ '../views/LibrariesView.vue'),
  },
  {
    path: '/play/:id',
    name: 'play',
    component: () => import(/* webpackChunkName: "libraries" */ '../views/PlayerView.vue'),
  },
  {
    path: '/item/:id',
    name: 'item',
    component: () => import(/* webpackChunkName: "libraries" */ '../views/ItemView.vue'),
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

export default router;
