import initSqlJs from 'sql.js';
import type {SqljsConnectionOptions} from 'typeorm/driver/sqljs/SqljsConnectionOptions';

export default async (): Promise<Partial<SqljsConnectionOptions>> => {
  return ({
    driver: initSqlJs,
    sqlJsConfig: {
      // Required to load the wasm binary asynchronously. Of course, you can host it wherever you want
      // You can omit locateFile completely when running in node
      locateFile: (file: string) => `https://cdnjs.cloudflare.com/ajax/libs/sql.js/1.6.2/${file}`,
    },
  });
};
